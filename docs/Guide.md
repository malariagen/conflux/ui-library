## Code examples

Throughout these docs you will see many code examples such as this:

```vue
<sa-button text="Click me" @click="alert('Yay!')" />
```

💡 The code above is editable in the browser and live updates the example. Try
changing the button text now.

You can copy and paste examples like that straight into your own Vue component
templates.

Some more complex examples look like this:

```vue
<template>
  <sa-button :text="'Clicked ' + count + ' times'" @click="increment" />
</template>

<script>
export default {
  data() {
    return {
      count: 0
    }
  },
  methods: {
    increment() {
      this.count++
    }
  }
}
</script>
```

This is also live editable but since it is a whole Vue component with its own
state, you will need to take care when copying and pasting.

## Vuex store

Each example has its own Vuex store. In order to keep the examples simple, the setup code is hidden.
