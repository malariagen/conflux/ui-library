> Note: we use the US spelling of 'color' to align with css `color`. 

## Swatches

The UI Library provides preset color variables which you can use in your applications. Colors are derived from the Sanger brand guidelines.

To maintain consistent styling, you should not use any other colors than provided here. If you use the sass color function below, your colors will stay in sync with any library updates.

```vue noeditor
<template>
  <color-swatches />
</template>
```

Use the `color(x)` function to apply a preset color from the swatches by inserting the color name in the brackets:
```scss
.element {
  color: color(primary);
}
```

## Accessibility

It is important you have sufficient color contrast for clear legibility of text.

The color of the text on the swatches above generally show contrast levels that meet AA accessibility standards:
- Dark colors can be used with both white and tinted colors and vice versa.
- Other colours generally work best with either white or very dark (black, primary dark, grey dark) colors as shown on the swatches above.
- The secondary brand colors do not all have great contrast so be careful when using with text.
