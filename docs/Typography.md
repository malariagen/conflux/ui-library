## Fonts

The UI Library uses the open source font, <a href="https://rsms.me/inter/" target="_blank">Inter</a>, as the default font. This is automatically applied throughout using a sass variable of `$font-family-primary`.

When using the library you should not need to use this variable. If other fonts are visible, make sure you do not have any overriding styles in your code.

```vue noeditor
<template>
  <typography-fonts />
</template>
```

## Sizes

The library comes with preset font sizes that you can use independently of heading tags.

Choose the correct heading tags (h1-h6) for the hierarchical structure of the content and use the preset sizes for styling; a `h1` tag should always be used for the main header, but a `h2` tag can be set larger visually.

Font size 1 is the default size for body text.

The preset font sizes are:
```vue noeditor
<template>
  <typography-sizes />
</template>
```

Use the `font-size(x)` function to apply a preset size from the scale:
```scss
.element {
  font-size: font-size(2);
}
```

## Weights

There are two font weights being used throughout the library, both with a sass variable.

The 'bold' weight (`$font-weight-bold`) is automatically applied to header tags (h1-h6) and may also be used to emphasise text.

The 'regular' weight (`$font-weight-regular`) is applied to body text.

```vue noeditor
<template>
  <typography-weights />
</template>
```

Use the variables to apply font-weight to an element:
```scss
.element {
  font-weight: $font-weight-bold;
}
```

## Line heights & letter spacing

There are two line height and letter spacing preset styles in the UI Library, all with sass variables.

The header line height (`$line-height-header`) and header letter spacing (`$letter-spacing-header`) presets provide tighter spacing for headers. These styles are applied automatically to all header tags (h1-h6).

The body line height (`$line-height-body`) and body letter spacing (`$letter-spacing-body`) presets provide more comfortable spacing for reading larger blocks of text and is already applied to paragraphs and lists.

```vue noeditor
<template>
  <typography-spacing />
</template>
```

Use the variables to apply line-height and letter-spacing to an element:
```scss
.element {
  line-height: $line-height-header;
  line-spacing: $line-spacing-header;
}
```

## Labels and alternative headers

We have a separate style class for field labels, table headings and other subheadings to distinguish from our normal header styles. This is usually set small and in grey.

You can append the `.label` class to any element to apply this style.

```
<label class="label">This is label</label>
```
