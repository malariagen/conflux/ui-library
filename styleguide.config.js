module.exports = {
  title: 'UI Library',
  sections: [
    { name: 'Introduction', content: 'docs/Home.md' },
    { name: 'Getting started', content: 'README.md' },
    { name: 'About these docs', content: 'docs/Guide.md' },
    { name: 'Color', content: 'docs/Color.md' },
    { name: 'Typography', content: 'docs/Typography.md' },
    {
      name: 'Components',
      content: 'src/components/README.md',
      components: 'src/components/**/*.vue',
      sectionDepth: 1
    }
  ],
  ribbon: {
    url: 'https://gitlab.com/malariagen/conflux/ui-library',
    text: 'Fork me on GitLab'
  },

  require: ['./styleguide/register.js'],
  renderRootJsx: './styleguide/componentRoot.js',
  ignore: [
    '**/src/components/_*/**',
    '**/{SaGridItem,SaSelectDateRangeInput}.vue'
  ],
  styleguidePublicPath: process.env.BASE_PATH,
  styleguideDir: 'dist-docs',

  pagePerSection: true,
  copyCodeButton: true,
  skipComponentsWithoutExample: true,
  exampleMode: 'expand',
  usageMode: 'expand',

  theme: {
    maxWidth: 800,
    sidebarWidth: 240,
    fontFamily: {
      base:
        "'Inter', -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Helvetica Neue', Arial, sans-serif"
    },
    fontSize: {
      base: 16,
      h1: 36,
      h2: 24,
      h3: 20,
      h4: 16,
      h5: 16,
      h6: 16
    },
    color: {
      base: '#232642',
      dark: '#333333',
      medium: '#666666',
      light: '#CCCCCC',
      lightest: '#F2F2F2',
      link: '#2E3B87',
      linkHover: '#2E3B87',
      border: '#F2F2F2',
      codeBackground: '#F2F2F2',
      sidebarBackground: '#FFFFFF'
    },
    space: [4, 8, 16, 32, 48, 64, 80],
    borderRadius: 4
  },

  styles: (theme) => ({
    StyleGuide: {
      logo: {
        paddingBottom: 0,
        borderBottom: 0
      }
    },
    Logo: {
      logo: {
        paddingTop: theme.space[4],
        fontSize: 24,
        fontWeight: 'bold'
      }
    },
    TableOfContents: {
      input: {
        borderColor: theme.color.light,
        '&::placeholder': {
          color: theme.color.dark
        }
      }
    },
    MarkdownHeading: {
      spacing: {
        margin: 0
      }
    },
    Heading: {
      heading: {
        fontWeight: 'bold',
        lineHeight: '1.25',
        letterSpacing: '-0.025em'
      },
      heading1: {
        marginTop: theme.space[4]
      },
      heading2: {
        marginTop: theme.space[4],
        borderTop: '1px solid ' + theme.color.light,
        paddingTop: theme.space[3],
        marginBottom: theme.space[3]
      },
      heading3: {
        marginTop: theme.space[3],
        marginBottom: theme.space[2]
      },
      heading4: {
        marginTop: theme.space[2],
        marginBottom: theme.space[1]
      }
    },
    Para: {
      para: {
        lineHeight: '1.5'
      }
    },
    Pathline: {
      pathline: {
        color: theme.color.medium
      }
    },
    ToolbarButton: {
      button: {
        color: theme.color.link
      }
    },
    Blockquote: {
      blockquote: {
        margin: theme.space[2] + ' 0',
        borderLeft: '1px solid ' + theme.color.border,
        paddingLeft: theme.space[2]
      }
    },
    Code: {
      code: {
        borderRadius: theme.borderRadius,
        padding: theme.borderRadius,
        fontSize: theme.fontSize.small,
        background: theme.color.codeBackground
      }
    },
    Table: {
      cellHeading: {
        color: theme.color.medium,
        fontSize: '12px',
        textTransform: 'uppercase',
        letterSpacing: '0.1em'
      }
    },
    Playground: {
      preview: {
        borderRadius: '4px 4px 0 0',
        borderBottom: 'none',
        // add spacing between components in an example
        '& > div > div > * + *': {
          marginTop: '1rem',
          // without this JSS breaks the display of some components like checkbox
          isolate: false
        },
        // utility class for aligning examples
        '& .right-align': {
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-end'
        },
        // make navbar and toolbar stay within the examples box
        '& .navbar, & .toolbar': {
          margin: '0'
        }
      },
      // hide the button to toggle the code editor
      controls: {
        display: 'none'
      }
    },
    ReactComponent: {
      // hide the button to toggle the props table
      tabButtons: {
        display: 'none'
      }
    }
  }),
  styleguideComponents: {
    SectionHeading: `${__dirname}/styleguide/overrides/Heading.jsx`
  }
}
