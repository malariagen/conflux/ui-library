import { mount } from '@vue/test-utils'
import SaNav from '@/components/nav/SaNav'

describe('SaNav', () => {
  test('renders with default props', () => {
    const wrapper = mount(SaNav)
    const nav = wrapper.find('nav')
    const navClasses = nav.classes()
    expect(navClasses).toStrictEqual(['nav', 'row', 'regular'])
    expect(navClasses).not.toContain('stacked')
    expect(navClasses).not.toContain('tabs')
    expect(navClasses).not.toContain('small')
    expect(navClasses).not.toContain('large')
  })
  test('renders stacked', () => {
    const wrapper = mount(SaNav, {
      propsData: {
        type: 'stacked'
      }
    })
    const nav = wrapper.find('nav')
    const navClasses = nav.classes()
    expect(navClasses).not.toContain('row')
    expect(navClasses).toContain('stacked')
    expect(navClasses).not.toContain('tabs')
  })
  test('renders tabs', () => {
    const wrapper = mount(SaNav, {
      propsData: {
        type: 'tabs'
      }
    })
    const nav = wrapper.find('nav')
    const navClasses = nav.classes()
    expect(navClasses).not.toContain('row')
    expect(navClasses).not.toContain('stacked')
    expect(navClasses).toContain('tabs')
  })
  test('renders light', () => {
    const wrapper = mount(SaNav, {
      propsData: {
        light: true
      }
    })
    const nav = wrapper.find('nav')
    const navClasses = nav.classes()
    expect(navClasses).toContain('light')
  })
  test('renders slot', () => {
    const wrapper = mount(SaNav, {
      slots: {
        default: '<p>test</p>'
      }
    })
    const p = wrapper.find('p')
    expect(p.exists()).toBe(true)
  })
})
