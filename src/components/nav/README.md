The `nav` component is used in combination with the `nav-item` component to
create navigation menus. 

## Row

### Small

```vue
<sa-nav size="small">
  <sa-nav-item href="#" active>Item 1</sa-nav-item>
  <sa-nav-item href="#">Item 2</sa-nav-item>
  <sa-nav-item href="#">Item 3</sa-nav-item>
</sa-nav>
```

### Regular

```vue
<sa-nav>
  <sa-nav-item href="#" active>Item 1</sa-nav-item>
  <sa-nav-item href="#">Item 2</sa-nav-item>
  <sa-nav-item href="#">Item 3</sa-nav-item>
</sa-nav>
```

### Large

```vue
<sa-nav size="large">
  <sa-nav-item href="#" active>Item 1</sa-nav-item>
  <sa-nav-item href="#">Item 2</sa-nav-item>
  <sa-nav-item href="#">Item 3</sa-nav-item>
</sa-nav>
```

### Light

```vue
<div style="background: #293DA3; padding: 1em;">
  <sa-nav light>
    <sa-nav-item href="#" active>Item 1</sa-nav-item>
    <sa-nav-item href="#">Item 2</sa-nav-item>
    <sa-nav-item href="#">Item 3</sa-nav-item>
  </sa-nav>
</div>
```

## Tabs

```vue
// works best against a background colour
<div style="background: #e9effc">
  <sa-nav type="tabs">
    <sa-nav-item href="#" active>Item 1</sa-nav-item>
    <sa-nav-item href="#">Item 2</sa-nav-item>
    <sa-nav-item href="#">Item 3</sa-nav-item>
  </sa-nav>
</div>
```

## Stacked

```vue
<sa-nav type="stacked">
  <sa-nav-item href="#" active>Item 1</sa-nav-item>
  <sa-nav-item href="#">Item 2</sa-nav-item>
  <sa-nav-item href="#">Item 3</sa-nav-item>
</sa-nav>
```
