A simple responsive grid layout. Uses flexbox to create 2-4 equal height cells per row, depending on viewport width. 

```vue
<sa-grid>
  <div style="border: solid; padding: 1em;">
    <h2>A heading</h2>
    <p>Some content</p>
  </div>
  <div style="border: solid; padding: 1em;">
    <h2>Another heading</h2>
    <p>
      Lots more content that makes things wrap onto another line so that we
      can see what it looks like in the example
    </p>
  </div>
  <div style="border: solid; padding: 1em;">
    <h2>Heading west</h2>
    <p>Over the ocean</p>
  </div>
</sa-grid>
```
