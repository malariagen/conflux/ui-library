import { mount } from '@vue/test-utils'
import SaGrid from '@/components/grid/SaGrid'

describe('SaGrid', () => {
  test('it works', () => {
    const wrapper = mount(SaGrid)
    expect(wrapper).toBeTruthy()
  })
  test('it renders the children', () => {
    const wrapper = mount(SaGrid, {
      slots: {
        default: '<p>A</p><p>B</p><p>C</p>'
      }
    })
    const items = wrapper.findAll('.item')
    expect(items.length).toBe(3)
    expect(items.at(0).text()).toBe('A')
    expect(items.at(1).text()).toBe('B')
    expect(items.at(2).text()).toBe('C')
  })
})
