Horizontal list of facts or figures.

```vue
const stats = { Height: '288cm', Width: '2km', Age: '23min' }

<sa-stats
  title="Vital statistics"
  :stats="stats"
/>
```
