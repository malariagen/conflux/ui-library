import { mount } from '@vue/test-utils'
import SaStats from '@/components/stats/SaStats'

describe('SaStats', () => {
  test('it works', () => {
    const wrapper = mount(SaStats, {
      propsData: {
        title: 'test'
      }
    })
    expect(wrapper).toBeTruthy()
  })
  test('renders the props properly', () => {
    const wrapper = mount(SaStats, {
      propsData: {
        title: 'test',
        stats: {
          A: 100,
          B: 99
        }
      }
    })
    const title = wrapper.find('.label')
    expect(title.text()).toBe('test')
    const stats = wrapper.findAll('.stat')
    expect(stats.length).toBe(2)
    expect(stats.at(0).text()).toBe('100 A')
    expect(stats.at(1).text()).toBe('99 B')
  })
})
