import { mount } from '@vue/test-utils'
import SaDropdown from '@/components/dropdown/SaDropdown'

describe('SaDropdown', () => {
  test('renders with default props', () => {
    const wrapper = mount(SaDropdown)
    const button = wrapper.find('.dropdown-button')
    const classes = button.classes()
    expect(classes).toContain('button')
    expect(classes).toContain('regular')
    expect(classes).toContain('primary')
  })
  test('opens and closes dropdown', async () => {
    const wrapper = mount(SaDropdown, {
      slots: {
        default: '<input type="text" class="dropdown-input" />'
      }
    })
    const wait = (ms) => new Promise((resolve) => setTimeout(resolve, ms))

    const button = wrapper.find('.dropdown-button')
    const dropdown = wrapper.find('.dropdown')
    const input = wrapper.find('.dropdown-input')
    expect(dropdown.element).not.toBeVisible()

    // open with button
    await button.trigger('click')
    expect(dropdown.element).toBeVisible()

    // close with button
    await button.trigger('click')
    expect(dropdown.element).not.toBeVisible()

    // open with button
    await button.trigger('click')
    expect(dropdown.element).toBeVisible()

    // focus child input (should not close dropdown)
    await input.element.focus()
    await wait(100)
    expect(dropdown.element).toBeVisible()

    // close by focusing on button
    await button.element.focus()
    await wait(100)
    expect(dropdown.element).toBeVisible()

    // reopen then close on focusout event
    await button.trigger('click')
    expect(dropdown.element).not.toBeVisible()
    await dropdown.trigger('focusout')
    expect(dropdown.element).not.toBeVisible()
  })
  test('stays open on inside click when staysOpen prop is true', async () => {
    const wrapper = mount(SaDropdown, {
      propsData: {
        staysOpen: true
      },
      slots: {
        default: '<input type="text" class="dropdown-input" />'
      }
    })
    const wait = (ms) => new Promise((resolve) => setTimeout(resolve, ms))

    const button = wrapper.find('.dropdown-button')
    const dropdown = wrapper.find('.dropdown')
    const input = wrapper.find('.dropdown-input')
    expect(dropdown.element).not.toBeVisible()

    // open with button
    await button.trigger('click')
    expect(dropdown.element).toBeVisible()

    // click inside dropdown expects to remain visible
    await input.trigger('click')
    await wait(100)
    // expect dropdown to stay open
    expect(dropdown.element).toBeVisible()

    // close with button
    await button.trigger('click')
    expect(dropdown.element).not.toBeVisible()
  })
})
