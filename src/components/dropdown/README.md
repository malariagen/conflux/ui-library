The `dropdown` component shows extra content when its button is pressed.

## Basic Dropdown

```vue
<sa-dropdown size="small" text="Click me">
  <div>This is the content</div>
</sa-dropdown>
```

## Dropdown Styles
```vue
<sa-dropdown size="small" avatar="Jane Doe" text="Select">
  <ul>
    <li class="option" tabindex="0">
      <a>Settings</a>
    </li>
    <li class="option" tabindex="0">
      <a>Log Out</a>
    </li>
  </ul>
</sa-dropdown>

<sa-dropdown
  type="secondary"
  size="small"
  text="Help"
  icon="help-circle"
  icon-only
>
  <div>
    <h1>Helpful Information</h1>
    <p>Help text.</p>
  </div>
</sa-dropdown>
```

## Sizes
```vue
<sa-dropdown size="small" text="Small">
  <div>Content</div>
</sa-dropdown>

<sa-dropdown text="Regular">
  <div>Content</div>
</sa-dropdown>

<sa-dropdown size="large" text="Large">
  <div>Content</div>
</sa-dropdown>
```

## Right aligned
```vue
<div class="right-align">
  <sa-dropdown size="small" text="Click me" align="right">
    <div>Content</div>
  </sa-dropdown>
</div>
```

## Stays open
```vue
<div class="remain-open">
  <sa-dropdown size="small" text="Click me" staysOpen="true">
    <div>remains open when interacted </div>
  </sa-dropdown>
</div>

<div>
  <sa-dropdown size="small" text="Click me">
    <div>Closes when interacted </div>
  </sa-dropdown>
</div>
```


