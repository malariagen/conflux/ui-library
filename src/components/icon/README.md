## Icon list

The UI Library uses open source icons from <a href="https://feathericons.com/" target="_blank">Feather Icons</a>.

You can use any icon name from the Feather Icons list (v4.28.0) in the icon prop of this component.

Here are some common icons for quick reference (ordered alphabetically):
```vue noeditor
<template>
  <icons-list />
</template>
```


## Examples

```vue
<sa-icon icon="check-circle" size="0" />
<sa-icon icon="check-circle" />
<sa-icon icon="check-circle" size="2" />
<sa-icon icon="check-circle" size="3" />
<sa-icon icon="check-circle" size="4" />
<sa-icon icon="check-circle" size="5" />
<sa-icon icon="check-circle" size="6" />
<sa-icon icon="loader" spin />
```
