import { mount } from '@vue/test-utils'
import SaIcon from '@/components/icon/SaIcon'

describe('SaIcon', () => {
  test('renders with icon prop', () => {
    const wrapper = mount(SaIcon, {
      propsData: {
        icon: 'check'
      }
    })
    const iconSvg = wrapper.find('svg')
    expect(iconSvg.classes()).toStrictEqual([
      'feather',
      'feather-check',
      'size-1'
    ])
  })
})
