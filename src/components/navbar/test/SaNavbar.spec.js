import { mount } from '@vue/test-utils'
import SaNavbar from '@/components/navbar/SaNavbar'

describe('SaNavbar', () => {
  test('renders with default props', () => {
    const wrapper = mount(SaNavbar)
    const nav = wrapper.find('nav')
    const classes = nav.classes()
    expect(classes).toStrictEqual(['navbar'])
    const appTitle = wrapper.find('.app')
    expect(appTitle.text()).toBe('App Title')
    const pageTitle = wrapper.find('.page')
    expect(pageTitle.text()).toBe('Page Title')
  })
  test('renders slots', () => {
    const wrapper = mount(SaNavbar, {
      slots: {
        right: '<p class="test-right">Right</p>',
        offcanvas: '<p class="test-offcanvas">Offcanvas</p>',
        offcanvasFooter: '<p class="test-offcanvas-footer">Offcanvas Footer</p>'
      }
    })
    const navRight = wrapper.find('.navbar-right')
    const right = navRight.find('.test-right')
    expect(right.exists()).toBe(true)
    expect(navRight.text()).toBe('Right')

    const navOff = wrapper.find('.offcanvas')
    const navOffContent = navOff.find('.content')
    const offcanvas = navOffContent.find('.test-offcanvas')
    expect(offcanvas.exists()).toBe(true)
    expect(offcanvas.text()).toBe('Offcanvas')

    const navOffFooter = navOff.find('.footer')
    const offcanvasFooter = navOffFooter.find('.test-offcanvas-footer')
    expect(offcanvasFooter.exists()).toBe(true)
    expect(offcanvasFooter.text()).toBe('Offcanvas Footer')
  })
  it('shows and hides menu', async () => {
    const wrapper = mount(SaNavbar, {
      slots: {
        offcanvas: '<p class="test-offcanvas">Offcanvas</p>'
      }
    })
    const offcanvas = wrapper.find('.test-offcanvas')
    expect(offcanvas.element).not.toBeVisible()
    const button = wrapper.find('.navbar-left button')
    await button.trigger('click')
    expect(offcanvas.element).toBeVisible()
    const background = wrapper.find('.background-fill')
    await background.trigger('click')
    expect(offcanvas.element).not.toBeVisible()
  })
  test('Test if button component exists if menuEnabled is true', () => {
    const wrapper = mount(SaNavbar, {
      propsData: {
        menuEnabled: true
      }
    })
    const button = wrapper.find('button')
    expect(button.exists()).toBe(true)
  })
  test('Test if button component does not exist if menuEnabled is false', () => {
    const wrapper = mount(SaNavbar, {
      propsData: {
        menuEnabled: false
      }
    })
    const button = wrapper.find('button')
    expect(button.exists()).toBe(false)
  })
})
