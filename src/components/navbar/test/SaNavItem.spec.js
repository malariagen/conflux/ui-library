import { mount } from '@vue/test-utils'
import SaNavItem from '@/components/nav/SaNavItem'

describe('SaNavItem', () => {
  test('renders with default props', () => {
    const wrapper = mount(SaNavItem)
    const li = wrapper.find('li')
    const classes = li.classes()
    expect(classes).toStrictEqual([])
    const text = wrapper.text()
    expect(text).toBe('')
  })
  test('renders active', () => {
    const wrapper = mount(SaNavItem, {
      propsData: {
        active: true
      }
    })
    const li = wrapper.find('li')
    const classes = li.classes()
    expect(classes).toStrictEqual(['active'])
  })
  test('renders link', () => {
    const wrapper = mount(SaNavItem, {
      propsData: {
        href: 'http://example.com'
      }
    })

    const a = wrapper.find('a')
    expect(a.exists()).toBe(true)
    expect(a.attributes()).toStrictEqual({ href: 'http://example.com' })
  })
  test('renders slot', () => {
    const wrapper = mount(SaNavItem, {
      slots: {
        default: '<p>test</p>'
      }
    })
    const p = wrapper.find('p')
    expect(p.exists()).toBe(true)
  })
})
