Main navigation and title bar for a web application.

```vue
<sa-navbar
  aria-label="Primary Navigation"
  app-title="App Title"
  page-title="Page Title"
>
  <template v-slot:right>
    <sa-dropdown
      type="secondary"
      size="small"
      align="right"
      avatar="Carrie Oakey"
      text="Carrie Oakey"
    >
      <ul>
        <li class="option" role="button">Log Out</li>
      </ul>
    </sa-dropdown>
  </template>
  <template v-slot:offcanvas>
    <sa-nav type="stacked" size="large" light>
      <sa-nav-item href="#">Item 1</sa-nav-item>
      <sa-nav-item href="#">Item 2</sa-nav-item>
      <sa-nav-item href="#">Item 3</sa-nav-item>
    </sa-nav>
  </template>
  <template v-slot:offcanvasFooter>
    <p>Nav Footer</p>
  </template>
</sa-navbar>
```
