The `button` component wraps the HTML `button` tag to provide consistent
styling.

```vue
<sa-button text="Click me" @click="alert('Clicked')" />
```

## Sizes

```vue
<sa-button size="small" text="Small Button" />
<sa-button text="Regular Button" />
<sa-button size="large" text="Large Button" />
```

## Button styles

```vue
<sa-button text="Primary Button" />
<sa-button text="Primary Disabled" disabled />
<sa-button text="Secondary Button" type="secondary" />
<sa-button text="Secondary Disabled" type="secondary" disabled />
<sa-button text="Danger Button" type="danger" />
<sa-button text="Danger Disabled" type="danger" disabled />
```

## Button types

```vue
<sa-button text="Avatar Button" avatar="Hugh Mann" />
<sa-button text="Icon Text Button" icon="check-circle" />
<sa-button text="Icon Button" icon="check-circle" icon-only />
<sa-button text="Icon Spin Button" icon="loader" spin />
```
