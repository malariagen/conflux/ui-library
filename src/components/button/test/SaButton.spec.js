import { mount, createWrapper } from '@vue/test-utils'
import SaButton from '@/components/button/SaButton'

describe('SaButton', () => {
  test('renders with default props', () => {
    const wrapper = mount(SaButton)
    const button = wrapper.find('button')
    const classes = button.classes()
    expect(classes).toContain('button')
    expect(classes).toContain('primary')
    expect(classes).toContain('regular')
    expect(classes).not.toContain('large')
  })
  test('disables button', () => {
    const wrapper = mount(SaButton, {
      propsData: {
        disabled: true
      }
    })
    const button = wrapper.find('button')
    expect(button.attributes('disabled')).toBe('disabled')
  })
  test('renders radio', () => {
    const wrapper = mount(SaButton, {
      propsData: {
        size: 'large',
        radio: true
      }
    })
    const button = wrapper.find('button')
    const classes = button.classes()
    expect(classes).toContain('button')
    expect(classes).toContain('large')
  })
  test('renders active radio', () => {
    const wrapper = mount(SaButton, {
      propsData: {
        radio: true,
        active: true
      }
    })
    const button = wrapper.find('button')
    const classes = button.classes()
    expect(classes).toContain('button')
  })
  test('renders avatar', () => {
    const wrapper = mount(SaButton, {
      propsData: {
        avatar: 'J Doe'
      }
    })
    expect(wrapper.find('.avatar').exists()).toBe(true)
    expect(wrapper.find('.text').exists()).toBe(true)
  })
  test('renders icon and text', () => {
    const wrapper = mount(SaButton, {
      propsData: {
        icon: 'check'
      }
    })
    expect(wrapper.find('.icon').exists()).toBe(true)
    expect(wrapper.find('.text').exists()).toBe(true)
    expect(wrapper.find('.sr-only').exists()).toBe(false)
  })
  test('renders icon only', () => {
    const wrapper = mount(SaButton, {
      propsData: {
        icon: 'check',
        iconOnly: true
      }
    })
    expect(wrapper.find('.icon').exists()).toBe(true)
    expect(wrapper.find('.text').exists()).toBe(false)
    expect(wrapper.find('.sr-only').exists()).toBe(true)
  })
  test('triggers modal event on click when id provided', () => {
    const wrapper = mount(SaButton, {
      propsData: {
        modalId: 'test-modal',
        text: 'click'
      }
    })
    const rootWrapper = createWrapper(wrapper.vm.$root)
    wrapper.trigger('click')
    expect(rootWrapper.emitted('modal-toggle')).toEqual([['test-modal']])
  })
  test('does not trigger modal event on click when no id provided', () => {
    const wrapper = mount(SaButton, {
      propsData: {
        text: 'click'
      }
    })
    const rootWrapper = createWrapper(wrapper.vm.$root)
    wrapper.trigger('click')
    expect(rootWrapper.emitted('modal-toggle')).toBeFalsy()
  })

  test('delays modal toggle event if click listener added', async () => {
    const onClick = jest.fn()
    const wrapper = mount(SaButton, {
      propsData: {
        modalId: 'test-modal',
        text: 'click'
      },
      listeners: {
        click: onClick
      }
    })
    const rootWrapper = createWrapper(wrapper.vm.$root)
    wrapper.trigger('click')
    expect(onClick).toHaveBeenCalled()
    expect(rootWrapper.emitted('modal-toggle')).toBeFalsy()
    await wrapper.vm.$nextTick()
    expect(rootWrapper.emitted('modal-toggle')).toBeFalsy()
    await wrapper.vm.$nextTick()
    expect(rootWrapper.emitted('modal-toggle')).toEqual([['test-modal']])
  })
})
