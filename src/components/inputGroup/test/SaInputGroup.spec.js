import { mount } from '@vue/test-utils'
import SaInputGroup from '@/components/inputGroup/SaInputGroup'

describe('SaInputGroup', () => {
  test('it works', () => {
    const wrapper = mount(SaInputGroup, {
      propsData: {
        label: 'Some input'
      }
    })
    expect(wrapper).toBeTruthy()
    expect(wrapper.find('.label').text()).toBe('Some input')
    expect(wrapper.find('.description').exists()).toBeFalsy()
    expect(wrapper.find('.status').exists()).toBeFalsy()
  })

  test('it shows error status correctly', () => {
    const wrapper = mount(SaInputGroup, {
      propsData: {
        label: 'Some input',
        description: 'Some description',
        status: 'An error',
        inError: true
      }
    })
    expect(wrapper.find('.label').text()).toBe('Some input')
    expect(wrapper.find('.description').text()).toBe('Some description')
    expect(wrapper.find('.status').text()).toBe('An error')
    expect(wrapper.find('.status').classes()).toContain('danger')
  })

  test('it shows success status correctly', () => {
    const wrapper = mount(SaInputGroup, {
      propsData: {
        label: 'Some input',
        description: 'Some description',
        status: 'Complete',
        inError: false
      }
    })
    expect(wrapper.find('.label').text()).toBe('Some input')
    expect(wrapper.find('.description').text()).toBe('Some description')
    expect(wrapper.find('.status').text()).toBe('Complete')
    expect(wrapper.find('.status').classes()).toContain('success')
  })
})
