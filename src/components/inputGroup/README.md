```vue
<sa-input-group label="Input group" description="Some descriptive text">
  <sa-input-field placeholder="Input" />
</sa-input-group>
```

## Variants

```vue
<sa-input-group label="With error" status="ERROR!!" in-error>
  <sa-input-field placeholder="Input" />
</sa-input-group>

<sa-input-group label="Successful" status="Operation complete">
  <sa-input-field placeholder="Input" />
</sa-input-group>
```
