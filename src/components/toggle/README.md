A collection of mutually-exclusive, clickable options.

```vue
<sa-toggle
  label="A label"
  :options="['This', 'That', 'The other']"
  initial-value="That"
/>
```

## Sizes

```vue
const options = ['Option 1', 'Option Two', 'Option the Third']

<sa-toggle label="Small"  size="small" :options="options" />
<sa-toggle label="Regular" :options="options" />
<sa-toggle label="Large" size="large" :options="options" />
```
