import { mount } from '@vue/test-utils'
import SaToggle from '@/components/toggle/SaToggle'

describe('SaToggle', () => {
  test('renders with minimal props', () => {
    const wrapper = mount(SaToggle, {
      propsData: {
        label: 'test'
      }
    })
    const buttons = wrapper.findAll('.sa-toggle .button')
    expect(buttons.exists()).toBe(false)
    const label = wrapper.findAll('label').at(0)
    expect(label.text()).toBe('test')
  })
  test('renders without initial value', () => {
    const wrapper = mount(SaToggle, {
      propsData: {
        label: 'test',
        options: [
          {
            value: 'a',
            label: 'A'
          },
          {
            value: 'b',
            label: 'B'
          }
        ]
      }
    })
    const options = wrapper.findAll('.sa-toggle .option')
    expect(options.length).toBe(2)
    const first = options.at(0)
    expect(first.text()).toBe('A')
    expect(first.classes()).toContain('active')
  })
  test('renders options', async () => {
    // const dispatch = jest.fn()
    const wrapper = mount(SaToggle, {
      propsData: {
        label: 'test',
        options: [
          {
            value: 'a',
            label: 'A'
          },
          {
            value: 'b',
            label: 'B'
          },
          'C'
        ],
        initialValue: 'b'
      }
    })

    const options = wrapper.findAll('.sa-toggle .option')
    expect(options.length).toBe(3)
    const first = options.at(0)
    expect(first.text()).toBe('A')
    expect(first.classes()).not.toContain('active')
    const second = options.at(1)
    expect(second.text()).toBe('B')
    expect(second.classes()).toContain('active')
    const third = options.at(2)
    expect(third.text()).toBe('C')
    expect(third.classes()).not.toContain('active')

    // change the active option
    await first.trigger('click')
    expect(first.classes()).toContain('active')
    expect(second.classes()).not.toContain('active')
    expect(third.classes()).not.toContain('active')
    expect(wrapper.emitted().change).toBeTruthy()
    expect(wrapper.emitted().change[0]).toEqual([{ value: 'a', label: 'A' }])
  })
})
