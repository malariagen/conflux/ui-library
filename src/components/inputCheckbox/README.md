## Checkbox variants

```js
<sa-input-checkbox disabled text="Disabled" />
<sa-input-checkbox in-error text="In error" />
<sa-input-checkbox initially-checked text="Initially checked" />
<sa-input-checkbox
  text="Long label. Etiam porta sem malesuada magna mollis euismod.
    Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit."
/>
```

## Checkbox sizes

```vue
<sa-input-checkbox size="small" text="Small" />
<sa-input-checkbox text="Medium" />
<sa-input-checkbox size="large" text="Large" />
```
