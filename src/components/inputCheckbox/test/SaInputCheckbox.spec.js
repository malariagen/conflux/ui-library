import { mount } from '@vue/test-utils'
import SaInputCheckbox from '@/components/inputCheckbox/SaInputCheckbox'

describe('SaInputCheckbox', () => {
  test('it updates on input change', async () => {
    const wrapper = mount(SaInputCheckbox, {
      propsData: {
        text: 'click me'
      }
    })
    expect(wrapper).toBeTruthy()
    expect(wrapper.classes()).not.toContain('checked')

    const input = wrapper.find('input')

    await input.trigger('input')
    expect(wrapper.find('input').element).not.toBeDisabled()

    expect(wrapper.classes()).toContain('checked')
    expect(wrapper.emitted().change).toBeTruthy()
    expect(wrapper.emitted().change[0]).toEqual([true])

    await input.trigger('input')

    expect(wrapper.classes()).not.toContain('checked')
    expect(wrapper.emitted().change[1]).toEqual([false])
  })
  test('it adds the right error class', () => {
    const wrapper = mount(SaInputCheckbox, {
      propsData: {
        text: 'click me',
        inError: true
      }
    })

    expect(wrapper).toBeTruthy()
    expect(wrapper.classes()).toContain('error')
  })
  test('it can be disabled', () => {
    const wrapper = mount(SaInputCheckbox, {
      propsData: {
        text: 'click me',
        disabled: true
      }
    })

    expect(wrapper).toBeTruthy()
    expect(wrapper.classes()).toContain('disabled')
    expect(wrapper.find('input').element).toBeDisabled()
  })
  test('can be initialised checked', () => {
    const wrapper = mount(SaInputCheckbox, {
      propsData: {
        text: 'click me',
        initiallyChecked: true
      }
    })

    expect(wrapper).toBeTruthy()
    expect(wrapper.classes()).toContain('checked')
  })
})
