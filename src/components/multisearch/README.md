The `multisearch` component is intended for selecting one or more items from
very large sets. Unlike the `select` component which loads all available options
into memory on the client, the `multisearch` component performs searches on the
server. This means it requires dedicated API endpoints and corresponding store
actions.

## Multisearch

```vue
<sa-multisearch filter-id="id_field" label="With label" />
<sa-multisearch
  filter-id="id_field"
  label="Initial selection"
  :initial-selection="['ABC-1.11', 'ABC-1.12']"
/>
```

## Sizes

```vue
<sa-multisearch filter-id="id_field" label="Small" size="small" />
<sa-multisearch filter-id="id_field" label="Regular" />
<sa-multisearch filter-id="id_field" label="Large" size="large" open />
<sa-multisearch filter-id="id_field" label="Full width" full-width />
```

## Right aligned

```vue
<div class="right-align">
  <sa-multisearch filter-id="id_field" align="right" />
</div>
```

## Store

Use the `createSearchStore` helper function to create a Vuex module to handle
the multisearch actions. It expects a dataloader object as an argument, and the
module must be named `searchFields`.

### Dataloader

The dataloader is responsible for fetching data from the API and must implement
the following methods.

#### fetch

Fetch should take a single argument:

| Name      | Type   | Description                                                                                                       |
| --------- | ------ | ----------------------------------------------------------------------------------------------------------------- |
| `filters` | object | An object with a single property where the key is the field ID to search for and the value is the term to search. |

The function signature is compatible with the `fetch` dataloader function for
the `table` component.

It should perform an exact match of the filters against the data and return an
object:

```js static
{
  meta: {},
  rows: [ ..., ... ]    // array of row objects
}
```

#### search

Search should take three arguments:

| Name      | Type    | Description                             |
| --------- | ------- | --------------------------------------- |
| `fieldId` | string  | The ID of the field to search           |
| `term`    | string  | The term to search for                  |
| `limit`   | integer | The maximum number of results to return |

It should perform a substring search on the term and return an object:

```js static
{
  meta: {
    total_results: ..., // total number of rows matched
  },
  rows: [ ..., ... ]    // array of row objects ordered by relevance
}
```

### Example implementation

```js static
import { createSearchStore } from '@sanger-surveillance-operations/ui-library'

const dataloader = {
  async fetch(filters) {
    const response = await axios.get('/fetch', filters)
    return {
      meta: {},
      rows: response.body.rows
    }
  },
  async search(fieldId, term, limit) {
    const response = await axios.get('/search', { limit, [fieldId]: term })
    return {
      meta: {
        total_results: response.body.total,
      },
      rows: response.body.rows
    }
  }
}

// root Vue instance
new Vue({
  ..., // existing setup code
  store: new Vuex.Store({
    modules: {
      searchFields: createSearchStore(dataLoader)
    }
  })
})
```
