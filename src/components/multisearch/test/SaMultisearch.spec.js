import lodash from 'lodash'
import { createLocalVue, mount } from '@vue/test-utils'
import Vuex from 'vuex'
import SaMultisearch from '../SaMultisearch'
import SaChip from '@/components/chip/SaChip'
import SaInputSearch from '@/components/inputSearch/SaInputSearch'
import createSearchStore from '@/store/createSearchStore'

const localVue = createLocalVue()
localVue.use(Vuex)

const wait = (ms) => new Promise((resolve) => setTimeout(resolve, ms))

function getChips(wrapper) {
  return wrapper.findAllComponents(SaChip).wrappers.map((w) => w.props())
}

describe('SaMultisearch', () => {
  const dataloader = { search: jest.fn(), fetch: jest.fn() }
  let store

  function createWrapper(propsData = {}) {
    return mount(SaMultisearch, {
      propsData: {
        filterId: 'some_field',
        ...propsData
      },
      store,
      localVue
    })
  }

  beforeEach(() => {
    jest.resetAllMocks()
    jest.spyOn(lodash, 'debounce').mockImplementation((cb) => cb)
    store = new Vuex.Store({
      modules: {
        searchFields: createSearchStore(dataloader)
      }
    })
  })

  test('opens and closes dropdown', async () => {
    const wrapper = createWrapper()
    const select = wrapper.find('.select')
    const dropdown = wrapper.find('.dropdown')
    expect(dropdown.element).not.toBeVisible()
    // open with button
    await select.trigger('click')
    expect(dropdown.element).toBeVisible()
    // close with button
    await select.trigger('click')
    expect(dropdown.element).not.toBeVisible()
    // open with button
    await select.trigger('click')
    expect(dropdown.element).toBeVisible()
    // close on focusout
    await dropdown.trigger('focusout')
    expect(dropdown.element).not.toBeVisible()
  })

  test('display props are respected', async () => {
    const wrapper = createWrapper({
      fullWidth: true,
      align: 'right',
      size: 'small'
    })

    const select = wrapper.find('.select')
    expect(select.classes()).toContain('full-width')
    expect(select.classes()).toContain('small')

    await select.trigger('click')
    const dropdown = wrapper.find('.dropdown')
    expect(dropdown.classes()).toContain('right')
  })

  test('initial values and button label', async () => {
    dataloader.fetch.mockResolvedValue({ rows: [{ some_field: 'One' }] })
    const wrapper = createWrapper({
      initialSelection: ['One', 'Two'],
      label: 'Field'
    })
    const button = wrapper.find('button')

    expect(getChips(wrapper)).toMatchObject([
      { type: 'primary', text: 'One' },
      { type: 'primary', text: 'Two' }
    ])
    expect(button.text()).toEqual('2 Field')
    expect(button.classes()).toContain('selected')

    await wait()
    expect(dataloader.fetch).toHaveBeenCalled()
    expect(getChips(wrapper)).toMatchObject([
      { type: 'danger', text: 'Two' },
      { type: 'success', text: 'One' }
    ])

    await wrapper
      .findComponent(SaChip)
      .find('.delete')
      .trigger('click')

    expect(getChips(wrapper)).toMatchObject([{ type: 'success', text: 'One' }])
    expect(button.text()).toEqual('One')

    await wrapper
      .findComponent(SaChip)
      .find('.delete')
      .trigger('click')

    expect(getChips(wrapper)).toHaveLength(0)
    expect(button.text()).toEqual('Field')
    expect(button.classes()).not.toContain('selected')
  })

  test('searching and selecting a suggestion', async () => {
    const suggestions = ['One', 'Once', 'Tone', 'Alone']
    dataloader.search.mockResolvedValue({
      rows: suggestions,
      meta: { total_results: 100 }
    })
    dataloader.fetch.mockResolvedValue({
      rows: [{ some_field: 'One' }]
    })

    const wrapper = createWrapper()
    const search = wrapper.findComponent(SaInputSearch).find('input')

    await search.setValue('on')
    expect(search.element.value).toEqual('on')

    await wait()

    const options = wrapper.findAll('[role=option]')
    expect(options.wrappers.map((w) => w.text())).toEqual(suggestions)

    await options.at(0).trigger('click')
    expect(wrapper.findAll('[role=option]')).toHaveLength(0)
    expect(getChips(wrapper)).toMatchObject([{ type: 'primary', text: 'One' }])

    const clearButton = wrapper.find('.header button')
    expect(clearButton.text()).toEqual('Clear')

    await clearButton.trigger('click')
    expect(search.element.value).toEqual('')
    expect(wrapper.findAllComponents(SaChip)).toHaveLength(0)
  })

  test('shows loading and no results messages', async () => {
    dataloader.search.mockResolvedValue({
      rows: [],
      meta: { total_results: 0 }
    })

    const wrapper = createWrapper()
    const search = wrapper.findComponent(SaInputSearch).find('input')

    await search.setValue('none')
    expect(wrapper.text()).toContain('Searching...')

    await wait()

    expect(wrapper.text()).not.toContain('Searching...')
    expect(wrapper.findAll('[role=option]')).toHaveLength(0)
    expect(wrapper.text()).toContain('No results')

    await search.setValue('')
    expect(wrapper.text()).not.toContain('Searching...')
    expect(wrapper.text()).not.toContain('No results')
  })

  test('handles comma separated input', async () => {
    dataloader.fetch.mockResolvedValue({ rows: [] })

    const wrapper = createWrapper()
    const search = wrapper.findComponent(SaInputSearch).find('input')

    await search.setValue('one,two,, two ,three ')
    expect(wrapper.text()).not.toContain('Searching...')
    expect(dataloader.search).not.toHaveBeenCalled()

    await search.trigger('keypress.enter')

    expect(getChips(wrapper)).toMatchObject([
      { type: 'primary', text: 'one' },
      { type: 'primary', text: 'two' },
      { type: 'primary', text: 'three' }
    ])
    expect(dataloader.fetch).toHaveBeenCalled()
  })

  describe('search API', () => {
    test('handles errors', async () => {
      dataloader.search.mockRejectedValue(new Error('meh'))

      const wrapper = createWrapper()
      const search = wrapper.findComponent(SaInputSearch).find('input')

      await search.setValue('some')
      expect(wrapper.text()).toContain('Searching...')

      await wait()

      expect(wrapper.text()).not.toContain('Searching...')
      expect(wrapper.text()).toContain('Unable to retrieve results')
    })

    test('ignores responses to stale requests', async () => {
      let resolveFirst
      let resolveSecond
      dataloader.search.mockReturnValueOnce(
        new Promise((resolve) => {
          resolveFirst = resolve
        })
      )
      dataloader.search.mockReturnValueOnce(
        new Promise((resolve) => {
          resolveSecond = resolve
        })
      )

      const wrapper = createWrapper()
      const search = wrapper.findComponent(SaInputSearch).find('input')

      await search.setValue('some')
      await search.setValue('someth')

      resolveSecond({
        rows: ['Something nice'],
        meta: { total_results: 1 }
      })

      await wait()

      expect(wrapper.findAll('[role=option]')).toHaveLength(1)

      resolveFirst({
        rows: ['Sometimes', 'Something'],
        meta: { total_results: 2 }
      })

      await wait()

      expect(wrapper.findAll('[role=option]')).toHaveLength(1)
      expect(wrapper.text()).not.toContain('Sometimes')
    })
  })

  describe('validate API', () => {
    test('handles errors', async () => {
      dataloader.fetch.mockRejectedValueOnce(new Error('moo'))
      dataloader.fetch.mockResolvedValueOnce({
        rows: [{ some_field: 'Thing' }]
      })

      const wrapper = createWrapper()
      const search = wrapper.findComponent(SaInputSearch).find('input')

      await search.setValue('Thing')
      await search.trigger('keypress.enter')

      expect(getChips(wrapper)).toMatchObject([
        { type: 'primary', text: 'Thing', disabled: false, loading: true }
      ])

      await wait()

      expect(getChips(wrapper)).toMatchObject([
        { type: 'primary', text: 'Thing', disabled: true, loading: false }
      ])
      expect(wrapper.text()).toContain('Unable to verify status')

      const retry = wrapper.find('[data-test-id=retry]')
      await retry.trigger('click')

      expect(getChips(wrapper)).toMatchObject([
        { type: 'primary', text: 'Thing', disabled: false, loading: true }
      ])

      await wait()

      expect(getChips(wrapper)).toMatchObject([
        { type: 'success', text: 'Thing', disabled: false, loading: false }
      ])
    })

    test('ignores responses to stale requests', async () => {
      let resolveFirst
      let resolveSecond
      dataloader.search.mockReturnValue(new Promise(() => {}))
      dataloader.fetch.mockReturnValueOnce(
        new Promise((resolve) => {
          resolveFirst = resolve
        })
      )
      dataloader.fetch.mockReturnValueOnce(
        new Promise((resolve) => {
          resolveSecond = resolve
        })
      )

      const wrapper = createWrapper()
      const search = wrapper.findComponent(SaInputSearch).find('input')

      await search.setValue('Something')
      await search.trigger('keypress.enter')
      await search.setValue('Sometimes')
      await search.trigger('keypress.enter')

      expect(getChips(wrapper)).toMatchObject([
        { type: 'primary', text: 'Sometimes' },
        { type: 'primary', text: 'Something' }
      ])

      resolveFirst({
        rows: [{ some_field: 'Something' }]
      })

      await wait()

      expect(getChips(wrapper)).toMatchObject([
        { type: 'primary', text: 'Sometimes' },
        { type: 'primary', text: 'Something' }
      ])

      resolveSecond({
        rows: [{ some_field: 'Something' }, { some_field: 'Sometimes' }]
      })

      await wait()

      expect(getChips(wrapper)).toMatchObject([
        { type: 'success', text: 'Sometimes' },
        { type: 'success', text: 'Something' }
      ])
    })
  })
})
