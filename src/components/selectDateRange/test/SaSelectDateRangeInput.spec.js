import { mount } from '@vue/test-utils'
import SaSelectDateRangeInput from '@/components/selectDateRange/SaSelectDateRangeInput'

describe('SaSelectDateRangeInput', () => {
  test('renders with default props', () => {
    const wrapper = mount(SaSelectDateRangeInput)

    expect(wrapper.text()).not.toContain('Clear')
  })

  test('renders with initial values', () => {
    const wrapper = mount(SaSelectDateRangeInput, {
      propsData: {
        label: 'test label',
        initialDate: '2020-03-01'
      }
    })

    expect(wrapper.text()).toContain('test label')
    const input = wrapper.find('.input input')
    expect(input.element.value).toEqual('01/03/2020')
  })

  test('when an initial value is given it shows the clear button', () => {
    const wrapper = mount(SaSelectDateRangeInput, {
      propsData: { initialDate: '2020-03-01' }
    })

    expect(wrapper.text()).toContain('Clear')
  })

  test('when the value is cleared is hides the clear button and emits and event', async () => {
    const wrapper = mount(SaSelectDateRangeInput, {
      propsData: { initialDate: '2020-03-01' }
    })

    expect(wrapper.text()).toContain('Clear')
    const clear = wrapper.find('.clear-date')
    await clear.trigger('click')
    expect(wrapper.text()).not.toContain('Clear')
    expect(wrapper.emitted().change).toEqual([[null]])
  })

  test('when a value is set it shows the clear button and triggers change event', async () => {
    const wrapper = mount(SaSelectDateRangeInput)

    expect(wrapper.text()).not.toContain('Clear')
    const input = wrapper.find('.input input')
    input.setValue('1/2/3030')
    await input.trigger('change')
    expect(wrapper.text()).toContain('Clear')
    expect(wrapper.emitted().change).toEqual([['3030-02-01']])
  })
})
