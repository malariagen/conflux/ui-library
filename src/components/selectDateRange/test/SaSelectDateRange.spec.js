import { mount } from '@vue/test-utils'
import SaSelectDateRange from '@/components/selectDateRange/SaSelectDateRange'

describe('SaSelectDateRange', () => {
  test('renders with default props', () => {
    const wrapper = mount(SaSelectDateRange)
    const select = wrapper.find('.select')
    const classes = select.classes()
    expect(classes).toContain('regular')

    const selects = wrapper.findAll('.select')
    selects.wrappers.forEach((select) => {
      expect(select.classes()).not.toContain('selected')
    })

    const mainSelect = wrapper.find('.sa-select-daterange > .select')
    expect(mainSelect.text()).toEqual('Date Range')
  })

  test('opens and closes dropdown', async () => {
    const wrapper = mount(SaSelectDateRange)
    const select = wrapper.find('.select')
    const dropdown = wrapper.find('.dropdown')
    expect(dropdown.element).not.toBeVisible()
    // open with button
    await select.trigger('click')
    expect(dropdown.element).toBeVisible()
    // close with button
    await select.trigger('click')
    expect(dropdown.element).not.toBeVisible()
    // open with button
    await select.trigger('click')
    expect(dropdown.element).toBeVisible()
    // TODO: test for closing on click outside of element
  })

  test('renders with initial values', () => {
    const year = new Date().getFullYear()
    const wrapper = mount(SaSelectDateRange, {
      propsData: {
        label: 'test date range',
        size: 'small',
        fields: ['test1', { value: 'test2', label: 'Test 2' }],
        initialRange: {
          field: 'test2',
          from: year + '-03-01',
          to: year + '-03-02'
        }
      }
    })

    const selects = wrapper.findAll('.select')
    selects.wrappers.forEach((select) => {
      expect(select.classes()).toContain('selected')
    })

    const mainSelect = wrapper.find('.sa-select-daterange > .select')
    expect(mainSelect.text()).toEqual('Test 2')
  })

  test('renders with no field options', () => {
    const year = new Date().getFullYear()
    const wrapper = mount(SaSelectDateRange, {
      propsData: {
        label: 'test date range',
        size: 'small',
        initialRange: {
          from: year + '-03-01',
          to: null
        }
      }
    })

    const fromSelects = wrapper.findAll('.from-select .select')
    fromSelects.wrappers.forEach((select) => {
      expect(select.classes()).toContain('selected')
    })
    const toSelects = wrapper.findAll('.to-select .select')
    toSelects.wrappers.forEach((select) => {
      expect(select.classes()).not.toContain('selected')
    })

    const mainSelect = wrapper.find('.sa-select-daterange > .select')
    expect(mainSelect.text()).toEqual(`1 Mar ${year} -`)
  })

  test('shows message with invalid date range', () => {
    const year = new Date().getFullYear()
    const wrapper = mount(SaSelectDateRange, {
      propsData: {
        label: 'test date range',
        size: 'small',
        fields: null,
        initialRange: {
          from: year + '-03-01',
          to: year - 1 + '-03-01'
        }
      }
    })

    const selects = wrapper.findAll('.select')
    selects.wrappers.forEach((select) => {
      expect(select.classes()).toContain('selected')
    })
    const dateRanges = wrapper.findAll('sa-date-range')
    dateRanges.wrappers.forEach((dateRange) => {
      expect(dateRange.classes()).toContain('invalid')
    })

    const message = wrapper.find('div.danger')
    expect(message.exists()).toBe(true)

    const mainSelect = wrapper.find('.sa-select-daterange > .select')
    expect(mainSelect.text()).toEqual(`1 Mar ${year} - 1 Mar ${year - 1}`)
    expect(mainSelect.classes()).toContain('error')
  })

  test('fires event with data after making selection and closing', () => {
    const year = new Date().getFullYear()
    const wrapper = mount(SaSelectDateRange, {
      propsData: {
        label: 'test date range',
        size: 'small',
        fields: ['test1', { value: 'test2', label: 'Test 2' }]
      }
    })

    const fieldSelect = wrapper.find('.field-select .sa-select')
    const fromDateSelect = wrapper.find('.from-select')
    const toDateSelect = wrapper.find('.to-select')

    fieldSelect.vm.$emit('change', ['test2'])
    fromDateSelect.vm.$emit('change', year + '-01-01')
    toDateSelect.vm.$emit('change', year + '-01-02')

    expect(wrapper.emitted().change).toBeTruthy()
    expect(wrapper.emitted().change.length).toEqual(3)
    expect(wrapper.emitted().change[2]).toEqual([
      { field: 'test2', from: year + '-01-01', to: year + '-01-02' }
    ])
  })

  test('does not fire event after making invalid selection and closing', () => {
    const year = new Date().getFullYear()
    const wrapper = mount(SaSelectDateRange, {
      propsData: {
        label: 'test date range',
        size: 'small'
      }
    })

    const fromDateSelect = wrapper.find('.from-select')
    const toDateSelect = wrapper.find('.to-select')

    fromDateSelect.vm.$emit('change', year + '-01-01')
    toDateSelect.vm.$emit('change', year - 1 + '-01-01')

    expect(wrapper.emitted().change).toBeTruthy()
    expect(wrapper.emitted().change.length).toEqual(1)
  })
})
