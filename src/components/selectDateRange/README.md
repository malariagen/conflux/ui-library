## Date Range

```vue
<sa-select-date-range />
```

## Date Range sizes

```vue
<sa-select-date-range size="small" label="Small Date Range" />
<sa-select-date-range label="Regular Date Range" />
<sa-select-date-range size="large" label="Large Date Range" />
```

## Date Range with values

```vue
<sa-select-date-range
  :initial-range="{
    from: '2020-01-01',
    to: '2020-02-01'
  }"
/>

<sa-select-date-range
  :initial-range="{
    from: '2020-02-01',
    to: '2020-01-01'
  }"
/>
```

## Date Range with fields

```vue
  <sa-select-date-range
    :fields="['Field A', 'Field B', 'Field C']"
    label="Date Range with fields"
  />
  
  <sa-select-date-range
    :fields="['Field A', 'Field B', 'Field C']"
    :initial-range="{
      field: 'Field B',
      from: '2020-01-01',
      to: '2020-07-29'
    }"
    label="Date Range with fields and initial range"
  />
```

## Right aligned

```vue
<div class="right-align">
  <sa-select-date-range align="right" />
</div>
```
