Simple flexbox container useful for toolbars.

```vue
<sa-toolbar>
  <template v-slot:left>
    <p>Toolbar left</p>
    <sa-select size="small" :options="['One thing']" />
  </template>
  <template v-slot:right>
    <sa-button size="small" text="Toolbar Right" />
  </template>
</sa-toolbar>
```
