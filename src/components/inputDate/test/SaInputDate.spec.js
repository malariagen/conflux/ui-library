import { mount } from '@vue/test-utils'
import SaInputDate from '@/components/inputDate/SaInputDate'

describe('SaInputDate', () => {
  test('it works', () => {
    const wrapper = mount(SaInputDate)
    const inputWrapper = wrapper.find('.input')
    expect(inputWrapper.classes()).not.toContain('disabled')
    expect(inputWrapper.classes()).not.toContain('error')
  })

  test('it renders disabled', () => {
    const wrapper = mount(SaInputDate, {
      propsData: {
        disabled: true
      }
    })
    const inputWrapper = wrapper.find('.input')
    expect(inputWrapper.classes()).toContain('disabled')
    expect(inputWrapper.find('input').attributes().disabled).toBe('disabled')
    expect(inputWrapper.find('button').attributes().disabled).toBe('disabled')
  })

  test('it shows the calendar when clicking the button', async () => {
    const wrapper = mount(SaInputDate)
    const button = wrapper.find('.input button')
    const picker = wrapper.find('duet-date-picker')

    // JSDOM 16.4 does not support shadow DOM so we have to mock
    // some of the date picker functionality
    picker.element.show = jest.fn()

    await button.trigger('click')
    expect(picker.element.show).toHaveBeenCalled()
  })

  test('it updates the input when selecting a date in the calendar', async () => {
    const wrapper = mount(SaInputDate)
    const input = wrapper.find('.input input')
    const picker = wrapper.find('duet-date-picker')

    await picker.trigger('duetChange', { detail: { value: '2020-02-01' } })
    expect(input.element.value).toBe('01/02/2020')
  })

  test('it updates calendar when inputting valid date', async () => {
    const wrapper = mount(SaInputDate)
    const input = wrapper.find('.input input')
    const picker = wrapper.find('duet-date-picker')

    await input.setValue('01/02/2020')
    await input.trigger('change')
    expect(picker.element.value).toBe('2020-02-01')
  })

  test('it updates calendar when a valid initialValue is provided', () => {
    const wrapper = mount(SaInputDate, {
      propsData: { initialValue: '01/02/2020' }
    })
    const picker = wrapper.find('duet-date-picker')

    expect(picker.element.value).toBe('2020-02-01')
  })

  describe('it emits change event when', () => {
    test('a date is selected', async () => {
      const wrapper = mount(SaInputDate)
      const input = wrapper.find('.input input')

      await input.setValue('1/2/2020')
      await input.trigger('change')
      expect(wrapper.emitted().change).toEqual([['2020-02-01']])
    })

    test('empty', async () => {
      const wrapper = mount(SaInputDate)
      const input = wrapper.find('.input input')

      await input.setValue('')
      await input.trigger('change')
      expect(wrapper.emitted().change).toEqual([['']])
    })
  })

  test.each`
    initialValue    | normalized
    ${'11/12/2014'} | ${'11/12/2014'}
    ${'1/2/2020'}   | ${'01/02/2020'}
    ${'31/1/1999'}  | ${'31/01/1999'}
    ${'10/1/2020'}  | ${'10/01/2020'}
    ${'1970-01-02'} | ${'02/01/1970'}
  `('normalises the input date format', ({ initialValue, normalized }) => {
    const wrapper = mount(SaInputDate, {
      propsData: { initialValue }
    })
    const inputWrapper = wrapper.find('.input')
    expect(inputWrapper.classes()).not.toContain('error')
    const input = wrapper.find('.input input')
    expect(input.element.value).toEqual(normalized)
  })

  describe('it shows error state when', () => {
    test('input is invalid and blurred', async () => {
      const wrapper = mount(SaInputDate)
      const inputWrapper = wrapper.find('.input')
      const input = wrapper.find('.input input')

      await input.setValue('1/2/3')
      await input.trigger('change')
      expect(inputWrapper.classes()).toContain('error')
    })

    test('parent component says so', () => {
      const wrapper = mount(SaInputDate, {
        propsData: { inError: true }
      })
      const input = wrapper.find('.input')
      expect(input.classes()).toContain('error')
    })

    test('initialValue is invalid', () => {
      const wrapper = mount(SaInputDate, {
        propsData: { initialValue: '1/2/3' }
      })

      const inputWrapper = wrapper.find('.input')
      expect(inputWrapper.classes()).toContain('error')

      const input = wrapper.find('.input input')
      expect(input.element.value).toEqual('1/2/3')
    })
  })

  describe('it clears error state when', () => {
    test('input is valid and blurred', async () => {
      const wrapper = mount(SaInputDate, {
        propsData: { initialValue: '1/2/3' }
      })
      const inputWrapper = wrapper.find('.input')
      const input = wrapper.find('.input input')

      expect(inputWrapper.classes()).toContain('error')
      await input.setValue('01/02/2020')
      await input.trigger('change')
      expect(inputWrapper.classes()).not.toContain('error')
    })

    test('input is empty and blurred', async () => {
      const wrapper = mount(SaInputDate, {
        propsData: { initialValue: '1/2/3' }
      })
      const inputWrapper = wrapper.find('.input')
      const input = wrapper.find('.input input')

      expect(inputWrapper.classes()).toContain('error')
      await input.setValue('')
      await input.trigger('change')
      expect(inputWrapper.classes()).not.toContain('error')
    })
  })
})
