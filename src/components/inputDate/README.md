## Sizes

```vue
<sa-input-date size="small" placeholder="Small" />
<sa-input-date placeholder="Regular" />
<sa-input-date size="large" placeholder="Large" />
```

## Variants

```vue
<sa-input-date disabled placeholder="This is disabled" />
<sa-input-date in-error placeholder="Error" />
```

## With initial value

```vue
<sa-input-date initial-value="01/01/2000" />
<sa-input-date initial-value="abcd" />
```
