The filter component is to be used in conjunction with the `table` component.
The table fields and filters are built from the data in the 'model'. The model
is usually provided by the server and stored in a Vuex store.

```vue
<template>
  <div>
    <sa-filter store-name="samples" :has-query-params="false" />
  </div>
</template>

<script>
export default {
  created() {
    // populate the dummy store with the dummy model
    this.$store.dispatch('samples/loadModel')
  }
}
</script>
```

This example uses dummy sample data already set up in the store.  


## Model

Example model:

```js static
{
  filters: [
    {
      fieldIds: ['field_1'],
      type: 'Select',
      options: [
        { value: true, label: 'True' },
        { value: false, label: 'False' }
      ]
    },
    {
      fieldIds: ['field_2'],
      type: 'MultiSelect',
      options: ['ABC-1', 'ABC-2', 'ABC-3', 'ABC-4']
    },
    {
      fieldIds: ['field_3'],
      type: 'Multisearch'
    },
    {
      fieldIds: ['date_field', 'event_field_start', 'event_field_end'],
      type: 'DateRange'
    },
  ],
  fields: [...]
}

```

The `filters` array tells the `filter` component which filters and options to display.

The `fields` array tells the table which columns to expect and match these
with the data (see the table component docs for more details).

### Filters

Properties used for filters in the `filters` array:

| Property   | Type                                                    | Description                                                                                  |
| ---------- | ------------------------------------------------------- | -------------------------------------------------------------------------------------------- |
| `fieldIds` | Array (required)                                        | This should contain the key/id(s) (found in the data) of the field(s) you want to filter on. |
| `type`     | String (required)                                       | This sets the type of filter (see Types table below).                                        |
| `options`  | Array (required when `type` is `Select`/ `MultiSelect`) | A list of options to filter by. Can optionally pass in an object with a `value` and `label`. |

### Types

Options for the filter `type`:

| Type             | Description                                                                                                                      |
| ---------------- | -------------------------------------------------------------------------------------------------------------------------------- |
| `Select`         | User can select a single option from the list of options.                                                                        |
| `Select_TA`      | A single select filter with a search input to filter options.                                                                    |
| `MultiSelect`    | User can select multiple options from the list of options.                                                                       |
| `MultiSelect_TA` | A multiselect filter with a search input to filter options.                                                                      |
| `Multisearch`    | User can search for multiple terms on given field (requires search API).                                                         |
| `DateRange`      | Filter date fields between two dates. Can pass in multiple `fieldIds` to allow selection of different date fields in one filter. |
