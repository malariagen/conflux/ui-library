import { mount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import { removeLineBreaks } from '../../../../test/utils'
import SaFilter from '@/components/filter/SaFilter'

function makeRouterOptions() {
  const localVue = createLocalVue()
  localVue.use(VueRouter)
  const router = new VueRouter()
  router.push({ query: null }) // ensure the query starts off empty
  return { localVue, router }
}

describe('SaFilter', () => {
  test('renders with default props', () => {
    const dispatch = jest.fn()
    const wrapper = mount(SaFilter, {
      ...makeRouterOptions(),
      propsData: {
        storeName: 'test'
      },
      mocks: {
        $store: {
          dispatch,
          state: {
            test: {
              model: {
                fields: []
              }
            }
          }
        }
      }
    })
    const toolbarItems = wrapper.findAll('.sa-filter')
    const filters = toolbarItems.at(0)
    expect(filters.find('select').exists()).toBe(false)
    expect(wrapper.vm.fetched).toBe(0)
  })

  test('findField behaves correctly', () => {
    const wrapper = mount(SaFilter, {
      ...makeRouterOptions(),
      propsData: {
        storeName: 'test'
      },
      mocks: {
        $store: {
          state: {
            test: {
              model: {
                fields: [
                  {
                    id: 'nested',
                    subfields: [
                      {
                        id: 'field',
                        title: 'Nested Field'
                      }
                    ]
                  },
                  {
                    title: 'Parent',
                    subfields: [
                      { id: 'child1', title: 'Child 1' },
                      { id: 'child2', title: 'Child 2' }
                    ]
                  },
                  {
                    id: 'prep',
                    title: 'Prep',
                    type: 'Event',
                    started: {
                      id: 'prep_start'
                    },
                    done: {
                      id: 'prep_done'
                    }
                  },
                  {
                    id: 'study',
                    title: 'Study',
                    subfields: [
                      { id: 'study_id', title: 'ID' },
                      { id: 'study_name', title: 'Name' }
                    ]
                  },
                  {
                    id: 'active',
                    title: 'Active'
                  },
                  {
                    id: 'date',
                    title: 'Date'
                  }
                ]
              }
            }
          }
        }
      }
    })

    const prepStarted = wrapper.vm.findField('prep_start')
    const prepDone = wrapper.vm.findField('prep_done')
    const studyName = wrapper.vm.findField('study_name')
    const child2 = wrapper.vm.findField('child2')
    expect(prepStarted).not.toBeNull()
    expect(prepStarted.title).toBe('Prep Start')
    expect(prepStarted.id).toBe('prep_start')
    expect(prepDone).not.toBeNull()
    expect(prepDone.title).toBe('Prep End')
    expect(prepDone.id).toBe('prep_done')
    expect(studyName).not.toBeNull()
    expect(studyName.title).toBe('Name')
    expect(studyName.id).toBe('study_name')
    expect(child2).not.toBeNull()
    expect(child2.title).toBe('Child 2')
    expect(child2.id).toBe('child2')
    expect(wrapper.vm.findField('field')).not.toBeNull()
    expect(wrapper.vm.findField('date')).not.toBeNull()
    expect(wrapper.vm.findField('missing')).toBeNull()
  })

  test('renders labels for selected filters, when enabled', () => {
    const dispatch = jest.fn()
    const tableState = {
      filters: {
        study_id: ['1']
      },
      model: {
        filters: [
          {
            fieldIds: ['study_id'],
            type: 'MultiSelect',
            options: ['1', '2']
          },
          {
            fieldIds: ['active'],
            type: 'Select',
            options: ['Yes', 'No']
          }
        ],
        fields: [
          {
            id: 'study_id',
            title: 'Study ID'
          },
          {
            id: 'active',
            title: 'Active'
          }
        ]
      }
    }
    const wrapper = mount(SaFilter, {
      ...makeRouterOptions(),
      propsData: {
        storeName: 'test',
        showNamesAboveSelection: true
      },
      mocks: {
        $store: {
          dispatch,
          state: {
            test: tableState
          }
        }
      }
    })

    const query = { study_id: ['1'] }
    wrapper.vm.$router
      .push(query)
      // silence a NavigationDuplicated error
      .catch(() => {})

    const labels = wrapper.findAll('.filter-label')
    expect(labels.length).toEqual(2)

    const visible = labels.filter((label) => label.classes('active'))
    expect(visible.length).toEqual(1)
    expect(visible.at(0).text()).toEqual('Study ID')
  })

  test('renders filters', () => {
    const dispatch = jest.fn()
    const tableState = {
      model: {
        filters: [
          {
            fieldIds: ['study_id'],
            type: 'MultiSelect',
            options: ['1', '2']
          },
          {
            fieldIds: ['active'],
            type: 'Select',
            options: ['Yes', 'No']
          },
          {
            fieldIds: ['date'],
            type: 'DateRange'
          }
        ],
        fields: [
          {
            id: 'id',
            title: 'ID'
          },
          {
            id: 'study_id',
            title: 'Study ID'
          },
          {
            id: 'active',
            title: 'Active'
          },
          {
            id: 'date',
            title: 'Date'
          }
        ]
      }
    }
    const wrapper = mount(SaFilter, {
      ...makeRouterOptions(),
      propsData: {
        storeName: 'test'
      },
      mocks: {
        $store: {
          state: {
            test: tableState
          },
          dispatch
        }
      }
    })
    const selects = wrapper.findAll('.sa-filter .filter-wrapper > .sa-select')
    expect(selects.length).toBe(2)
    const multiSelect = selects.at(0)
    expect(multiSelect.find('.single').exists()).toBe(false)
    expect(multiSelect.find('.multi').exists()).toBe(true)
    const select = selects.at(1)
    expect(select.find('.single').exists()).toBe(true)
    expect(select.find('.multi').exists()).toBe(false)
    const daterangeButton = wrapper.find(
      '.sa-filter .sa-select-daterange > .select'
    )
    expect(daterangeButton.text()).toBe('Date Range')
  })

  test('displays row and meta information', async () => {
    const tableState = {
      meta: {
        total_filtered_results: 0,
        total_results: 0
      },
      model: {
        filters: [],
        fields: []
      },
      rows: null
    }
    const wrapper = mount(SaFilter, {
      ...makeRouterOptions(),
      propsData: {
        storeName: 'test',
        disableColumnGroups: true
      },
      mocks: {
        $store: {
          state: {
            test: tableState
          },
          dispatch: jest.fn()
        }
      }
    })
    const metaRow = wrapper.find('.meta')
    expect(removeLineBreaks(metaRow.text())).toBe('0 results')
    tableState.meta = {
      total_filtered_results: 3,
      total_results: 4
    }
    tableState.rows = { length: 3 }
    await wrapper.vm.$nextTick()
    expect(removeLineBreaks(metaRow.text())).toBe('3 results out of 4')
  })

  test('ignores invalid filters', () => {
    const tableState = {
      model: {
        filters: [
          {
            fieldIds: [],
            type: 'MultiSelect',
            options: ['1', '2']
          },
          {
            fieldIds: ['active!'],
            type: 'Select',
            options: ['Yes', 'No']
          },
          {
            fieldIds: ['invalid_date'],
            type: 'DateRange'
          }
        ],
        fields: [
          {
            id: 'id',
            title: 'ID'
          },
          {
            id: 'study_id',
            title: 'Study ID'
          },
          {
            id: 'active',
            title: 'Active'
          },
          {
            id: 'date',
            title: 'Date'
          }
        ]
      }
    }
    const dispatch = jest.fn()
    const wrapper = mount(SaFilter, {
      ...makeRouterOptions(),
      propsData: {
        storeName: 'test'
      },
      mocks: {
        $store: {
          state: {
            test: tableState
          },
          dispatch
        }
      }
    })
    const selects = wrapper.findAll('.sa-filter .filter-wrapper > .sa-select')
    expect(selects.length).toEqual(0)
    const dateranges = wrapper.findAll(
      '.sa-filter .filter-wrapper > .sa-select-daterange'
    )
    expect(dateranges.length).toEqual(0)
  })

  test('it syncs with vue router url params, when enabled', async () => {
    const dispatch = jest.fn()
    const tableState = {
      filters: {
        a: ['2'],
        bool: [true],
        myDateRange: {
          field: 'date',
          from: '2020-01-01',
          to: null
        }
      },
      model: {
        fields: [
          {
            id: 'a',
            title: 'A Field'
          },
          {
            id: 'bool',
            title: 'A Boolean'
          },
          {
            id: 'date',
            title: 'A Date'
          },
          {
            id: 'date2',
            title: 'A Date 2'
          }
        ],
        filters: [
          {
            fieldIds: ['a'],
            type: 'MultiSelect',
            options: ['1', '2', '3']
          },
          {
            fieldIds: ['bool'],
            type: 'Select',
            options: [
              { value: true, label: 'True' },
              { value: false, label: 'False' }
            ]
          },
          {
            fieldIds: ['date', 'date2'],
            type: 'DateRange'
          }
        ]
      }
    }

    const { localVue, router } = makeRouterOptions()
    await router.push({
      query: {
        a: ['2'],
        bool: 'true',
        ignored: ['should be ignored'],
        myDateRange_field: 'date',
        myDateRange_from: '2020-01-01'
      }
    })
    const wrapper = mount(SaFilter, {
      localVue,
      router,
      propsData: {
        storeName: 'test',
        dateRangeKey: 'myDateRange'
      },
      mocks: {
        $store: {
          dispatch,
          state: {
            test: tableState
          }
        }
      }
    })

    // check filter params have set filters
    expect(dispatch).toBeCalledWith('test/setFilters', {
      a: ['2'],
      bool: [true],
      myDateRange: {
        field: 'date',
        from: '2020-01-01',
        to: null
      }
    })

    // check selections are present on frontend
    const selectButtons = wrapper.findAll(
      '.filter-wrapper > .sa-select > .select'
    )
    expect(selectButtons.at(0).text()).toBe('2')
    expect(selectButtons.at(1).text()).toBe('True')
    const daterangeButton = wrapper.find(
      '.sa-filter .sa-select-daterange > .select'
    )
    expect(daterangeButton.text()).toBe('A Date')

    // Select another option in multiselect
    await selectButtons.at(0).trigger('click')
    const dropdown = wrapper.find('.dropdown')
    const options = dropdown.findAll('.option')
    expect(options.length).toBe(3)
    await options.at(0).trigger('click')
    expect(dispatch).toBeCalledWith('test/setFilters', {
      a: ['1', '2'],
      bool: [true],
      myDateRange: {
        field: 'date',
        from: '2020-01-01',
        to: null
      }
    })

    // clear multiselect
    const clearButton = dropdown.find('.clear-select')
    expect(clearButton.text()).toBe('Clear')
    await clearButton.trigger('click')
    expect(dispatch).toBeCalledWith('test/setFilters', {
      bool: [true],
      myDateRange: {
        field: 'date',
        from: '2020-01-01',
        to: null
      }
    })

    // change boolean select
    await selectButtons.at(1).trigger('click')
    const boolDropdown = wrapper.findAll('.dropdown').at(1)
    const boolOptions = boolDropdown.findAll('.option')
    expect(boolOptions.length).toBe(2)
    await boolOptions.at(1).trigger('click')
    expect(dispatch).toBeCalledWith('test/setFilters', {
      bool: [false],
      myDateRange: {
        field: 'date',
        from: '2020-01-01',
        to: null
      }
    })

    // change daterange
    await daterangeButton.trigger('click')
    const fieldSelect = wrapper.find('.field-select')
    const fieldOptions = fieldSelect.findAll('.option')
    expect(fieldOptions.length).toBe(2)
    await fieldOptions.at(1).trigger('click')
    expect(dispatch).toBeCalledWith('test/setFilters', {
      bool: [false],
      myDateRange: {
        field: 'date2',
        from: '2020-01-01',
        to: null
      }
    })
  })

  test('it waits for a model, if none present, before syncing with vue router url params', async () => {
    const dispatch = jest.fn()
    const tableState = {
      filters: { a: ['1'] },
      model: null
    }

    const { localVue, router } = makeRouterOptions()
    await router.push({ query: { a: '2' } })
    const wrapper = mount(SaFilter, {
      localVue,
      router,
      propsData: {
        storeName: 'test'
      },
      mocks: {
        $store: {
          dispatch,
          state: {
            test: tableState
          }
        }
      }
    })

    expect(dispatch).not.toBeCalled()
    tableState.model = {
      fields: [
        {
          id: 'a',
          title: 'A Field'
        }
      ],
      filters: [
        {
          fieldIds: ['a'],
          type: 'MultiSelect',
          options: ['1', '2', '3']
        }
      ]
    }
    await wrapper.vm.$nextTick()
    expect(dispatch).toBeCalledWith('test/setFilters', { a: ['2'] })
  })

  test('filter is not updated if all query params are invalid', async () => {
    const dispatch = jest.fn()
    const tableState = {
      filters: { a: ['1'] },
      model: {
        fields: [
          {
            id: 'b',
            title: 'A Field'
          }
        ],
        filters: [
          {
            fieldIds: ['b'],
            type: 'MultiSelect',
            options: ['1', '2', '3']
          }
        ]
      }
    }

    const { localVue, router } = makeRouterOptions()
    await router.push({ query: { a: ['1'] } })
    mount(SaFilter, {
      localVue,
      router,
      propsData: {
        storeName: 'test'
      },
      mocks: {
        $store: {
          dispatch,
          state: {
            test: tableState
          }
        }
      }
    })

    expect(dispatch).toHaveBeenCalledWith('test/setFilters', {})
  })

  test("it doesn't do anything with url params, when disabled", async () => {
    const { localVue, router } = makeRouterOptions()
    const dispatch = jest.fn()
    const tableState = {
      filters: { a: ['1'] },
      model: {
        fields: [
          {
            id: 'a',
            title: 'A Field'
          }
        ],
        filters: [
          {
            fieldIds: ['a'],
            type: 'MultiSelect',
            options: ['1', '2', '3']
          }
        ]
      }
    }

    await router.push({ query: { a: ['2'] } })
    const wrapper = mount(SaFilter, {
      localVue,
      router,
      propsData: {
        storeName: 'test',
        hasQueryParams: false
      },
      mocks: {
        $store: {
          dispatch,
          state: {
            test: tableState
          }
        }
      }
    })

    expect(dispatch).not.toBeCalled()
    tableState.filters = { a: ['3'] }
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$route.query).toEqual({ a: ['2'] })
  })
})
