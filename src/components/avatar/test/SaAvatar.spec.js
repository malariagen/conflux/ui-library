import { mount } from '@vue/test-utils'
import SaAvatar from '@/components/avatar/SaAvatar'

describe('SaAvatar', () => {
  test('renders with default props', () => {
    const wrapper = mount(SaAvatar, {
      propsData: {
        fullName: 'Jane'
      }
    })
    const avatar = wrapper.find('.avatar')
    const classes = avatar.classes()
    expect(classes).toContain('regular')
  })
  describe('Name transforms', () => {
    test('1 initial created from one word name', () => {
      const wrapper = mount(SaAvatar, {
        propsData: {
          fullName: 'Jane'
        }
      })
      const initials = wrapper.find('.initials')
      expect(initials.element).toContainHTML('J')
    })
    test('2 initials created from two word name', () => {
      const wrapper = mount(SaAvatar, {
        propsData: {
          fullName: 'Jane Doe'
        }
      })
      const initials = wrapper.find('.initials')
      expect(initials.element).toContainHTML('JD')
    })
    test('2 initials created from three word name using last name', () => {
      const wrapper = mount(SaAvatar, {
        propsData: {
          fullName: 'Jane X Doe'
        }
      })
      const initials = wrapper.find('.initials')
      expect(initials.element).toContainHTML('JD')
    })
  })
})
