The avatar component represents a user by their initials. It can also be used by
other components such as the `button` and `chip`.

```vue
<sa-avatar full-name="Joe" />
<sa-avatar full-name="Guy-Manuel de Homem-Christo" />
```

## Sizes

```vue
<sa-avatar size="small" full-name="Joe Bloggs" />
<sa-avatar full-name="Joe Bloggs" />
<sa-avatar size="large" full-name="Joe Bloggs" />
```
