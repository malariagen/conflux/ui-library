import { mount } from '@vue/test-utils'
import SaCaption from '@/components/caption/SaCaption'

describe('SaCaption', () => {
  test('it shows nothing with no text', () => {
    const wrapper = mount(SaCaption)
    expect(wrapper).toBeTruthy()
    expect(wrapper.text()).toBe('')
  })

  test('it shows no icon for normal captions', () => {
    const wrapper = mount(SaCaption, {
      propsData: {
        text: 'normal caption'
      }
    })
    const icon = wrapper.find('.icon')
    expect(icon.exists()).toBeFalsy()
  })

  test('it shows icon for danger captions', () => {
    const wrapper = mount(SaCaption, {
      propsData: {
        text: 'danger',
        type: 'danger'
      }
    })
    expect(wrapper.classes()).toContain('danger')
    const icon = wrapper.find('.icon')
    expect(icon.exists()).toBeTruthy()
  })

  test('it shows icon for success captions', () => {
    const wrapper = mount(SaCaption, {
      propsData: {
        text: 'complete',
        type: 'success'
      }
    })
    expect(wrapper.classes()).toContain('success')
    const icon = wrapper.find('.icon')
    expect(icon.exists()).toBeTruthy()
  })
  test('it shows slot content', () => {
    const wrapper = mount(SaCaption, {
      propsData: {
        text: 'fallback'
      },
      slots: {
        default: 'Text and <button>btn</button>'
      }
    })
    expect(wrapper.find('button').exists()).toBeTruthy()
    expect(wrapper.text()).toBe('Text and btn')
  })
})
