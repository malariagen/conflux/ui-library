The caption component can get its content from either the `text` attribute or a
slot.

```vue
<sa-caption text="A plain text caption" />
<sa-caption>
  A rich caption with <strong>formatting</strong>
</sa-caption>
```

## Caption styles

```vue
<sa-caption text="A normal caption" />
<sa-caption text="A danger caption" type="danger" />
<sa-caption text="A success caption" type="success" />
```
