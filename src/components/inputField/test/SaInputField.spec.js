import { mount } from '@vue/test-utils'
import SaInputField from '@/components/inputField/SaInputField'

describe('SaInputText', () => {
  test('it works', () => {
    const wrapper = mount(SaInputField)
    expect(wrapper).toBeTruthy()
  })
  test('it renders with default props', () => {
    const wrapper = mount(SaInputField)
    const input = wrapper.find('.input')
    expect(input.classes()).toContain('regular')
    expect(input.element.type).toBe('text')
  })
  test('it renders disabled class', () => {
    const wrapper = mount(SaInputField, {
      propsData: {
        disabled: true
      }
    })
    const input = wrapper.find('.input')
    expect(input.classes()).toContain('disabled')
  })
  test('it renders error class', () => {
    const wrapper = mount(SaInputField, {
      propsData: {
        inError: true
      }
    })
    const input = wrapper.find('.input')
    expect(input.classes()).toContain('error')
  })
})
