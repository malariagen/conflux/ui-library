## Input Field Sizes

```vue
<sa-input-field size="small" placeholder="Small" />
<sa-input-field placeholder="Regular" />
<sa-input-field size="large" placeholder="Large" />
```

## Input Field Variants

```vue
<sa-input-field disabled placeholder="This is disabled" />
<sa-input-field in-error placeholder="Error" />
```

## Input Field Types

```vue
<sa-input-field type="text" placeholder="Text" />
<sa-input-field type="number" placeholder="Number" />
<sa-input-field type="email" placeholder="Email" />
<sa-input-field type="tel" placeholder="Tel" />
<sa-input-field type="password" placeholder="Password" />
```
