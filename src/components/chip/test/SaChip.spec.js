import { mount } from '@vue/test-utils'
import SaChip from '@/components/chip/SaChip'

describe('SaChip', () => {
  test('renders with default settings', async () => {
    const wrapper = mount(SaChip, {
      propsData: {
        text: 'Test Chip'
      }
    })
    // chip has expected classes
    const chip = wrapper.find('.chip')
    expect(chip.classes()).toStrictEqual(['chip', 'primary', 'regular'])
    // main button
    const mainButton = wrapper.find('.main')
    // button has text
    const mainButtonText = mainButton.find('.text')
    expect(mainButtonText.text()).toBe('Test Chip')
    // aria-pressed should be set to false
    expect(mainButton.attributes('aria-pressed')).toBe('false')
    // aria-disabled should be set to true
    expect(mainButton.attributes('aria-disabled')).toBe('true')
    // on click nothing emitted
    await mainButton.trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.emitted()).toEqual({})
    // does not have delete button
    expect(wrapper.find('.delete').exists()).not.toBeTruthy()
  })
  test('check default selected settings', () => {
    const wrapper = mount(SaChip, {
      propsData: {
        selected: true
      }
    })
    // chip has selected class
    const chip = wrapper.find('.chip')
    expect(chip.classes()).toContain('selected')
    // main button aria-pressed should be set to true
    const mainButton = wrapper.find('.main')
    expect(mainButton.attributes('aria-pressed')).toBe('true')
  })
  test('can toggle', async () => {
    const wrapper = mount(SaChip, {
      propsData: {
        canToggle: true
      }
    })
    // chip has toggle class
    const chip = wrapper.find('.chip')
    expect(chip.classes()).toContain('toggle')
    expect(chip.classes()).not.toContain('selected')
    // main button
    const mainButton = wrapper.find('.main')
    // aria-pressed should be set to false
    expect(mainButton.attributes('aria-pressed')).toBe('false')
    // aria-disabled should not exist
    expect(mainButton.attributes()).not.toContain('aria-disabled')
    // on click
    await mainButton.trigger('click')
    await wrapper.vm.$nextTick()
    // - emits toggle
    expect(wrapper.emitted().toggle.length).toBe(1)
    // - sets aria-pressed to true
    expect(mainButton.attributes('aria-pressed')).toBe('true')
    // - adds selected class
    expect(chip.classes()).toContain('selected')
    // on second click
    await mainButton.trigger('click')
    await wrapper.vm.$nextTick()
    // - emits second toggle
    expect(wrapper.emitted().toggle.length).toBe(2)
    // - sets aria-pressed to false
    expect(mainButton.attributes('aria-pressed')).toBe('false')
    // - removes selected class
    expect(chip.classes()).not.toContain('selected')
  })
  test('can delete', async () => {
    const wrapper = mount(SaChip, {
      propsData: {
        canDelete: true,
        text: 'Test Chip'
      }
    })
    // delete button
    const deleteButton = wrapper.find('.delete')
    // includes screenreader text
    const srText = wrapper.find('.sr-only')
    expect(srText.text()).toBe('Delete Test Chip')
    // on click emits delete
    await deleteButton.trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.emitted().delete).toBeTruthy()
  })
  test('sets loading settings', async () => {
    const wrapper = mount(SaChip, {
      propsData: {
        loading: true,
        canToggle: true,
        selected: true
      }
    })
    // chip has loading class and no toggle or selected class
    const chip = wrapper.find('.chip')
    expect(chip.classes()).toContain('loading')
    expect(chip.classes()).not.toContain('toggle')
    expect(chip.classes()).not.toContain('selected')
    // main button
    const mainButton = wrapper.find('.main')
    // aria-disabled should be set to true
    expect(mainButton.attributes('aria-disabled')).toBe('true')
    // aria-pressed should not exist
    expect(mainButton.attributes()).not.toContain('aria-pressed')
    // on click nothing emitted and class not selected
    await mainButton.trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.emitted()).toEqual({})
    expect(chip.classes()).not.toContain('selected')
  })
  test('sets disabled settings', async () => {
    const wrapper = mount(SaChip, {
      propsData: {
        disabled: true,
        canToggle: true,
        canDelete: true,
        selected: true
      }
    })
    // chip has disabled class and no toggle or selected class
    const chip = wrapper.find('.chip')
    expect(chip.classes()).toContain('disabled')
    expect(chip.classes()).not.toContain('toggle')
    expect(chip.classes()).not.toContain('selected')
    // main button
    const mainButton = wrapper.find('.main')
    // disabled should be set to disabled
    expect(mainButton.attributes('disabled')).toBe('disabled')
    // aria-disabled should not exist
    expect(mainButton.attributes()).not.toContain('aria-disabled')
    // aria-pressed should not exist
    expect(mainButton.attributes()).not.toContain('aria-pressed')
    // on click nothing emitted and class not selected
    await mainButton.trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.emitted()).toEqual({})
    expect(chip.classes()).not.toContain('selected')
    // clicking on delete nothing emitted
    const deleteButton = wrapper.find('.delete')
    await deleteButton.trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.emitted()).toEqual({})
  })
})
