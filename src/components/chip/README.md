The `chip` component both displays information and can be an input. 

## Basic chip

```vue
<sa-chip text="This is a chip" />
```

## Chip states

```vue
<div
  v-for="[id, title, icon] in [
    ['primary', 'Primary', 'info'],
    ['success', 'Success', 'check-circle'],
    ['danger', 'Danger', 'x-circle']
  ]"
  :key="id"
>
  <h3>{{ title }} Chips</h3>
  <sa-chip text="Default" :type="id" />
  <sa-chip text="Deletable" :type="id" can-delete />
  <sa-chip text="Icon" :type="id" :icon="icon" />
  <sa-chip text="Icon Deletable" :type="id" :icon="icon" can-delete />
  <sa-chip text="Avatar" :type="id" avatar="Jane Doe" />
  <sa-chip text="Avatar Deletable" :type="id" avatar="Jane Doe" can-delete />
  <sa-chip text="Selected" :type="id" selected />
  <sa-chip text="Selected Deletable" :type="id" selected can-delete />
  <sa-chip text="Toggle" :type="id" can-toggle />
  <sa-chip text="Toggle Deletable" :type="id" can-toggle can-delete />
  <sa-chip text="Toggle Selected" :type="id" can-toggle selected />
</div>
```

## Sizes

```vue
<div>
  <sa-chip text="Small" size="small" />
  <sa-chip text="Small Deletable" size="small" can-delete />
  <sa-chip text="Small Icon" size="small" icon="info" />
  <sa-chip text="Small Avatar" size="small" avatar="Jane Doe" />
</div>
<div>
  <sa-chip text="Regular" />
  <sa-chip text="Regular Deletable" can-delete />
  <sa-chip text="Regular Icon" icon="info" />
  <sa-chip text="Regular Avatar" avatar="Jane Doe" />
</div>
<div>
  <sa-chip text="Large" size="large" />
  <sa-chip text="Large Deletable" size="large" can-delete />
  <sa-chip text="Large Icon" size="large" icon="info" />
  <sa-chip text="Large Avatar" size="large" avatar="Jane Doe" />
</div>
```

## Loading

```vue
<sa-chip text="Loading Small" size="small" loading selected can-toggle />
<sa-chip text="Loading Regular" loading selected can-toggle />
<sa-chip text="Loading Large" size="large" loading selected can-toggle />
<sa-chip text="Loading Deletable" loading can-delete />
```

## Disabled

```vue
<sa-chip text="Disabled Small" size="small" disabled selected can-toggle />
<sa-chip text="Disabled Regular" disabled selected can-toggle />
<sa-chip text="Disabled Large" size="large" disabled selected can-toggle />
<sa-chip text="Disabled Icon" disabled icon="info" />
<sa-chip text="Disabled Avatar" disabled avatar="Jane Doe" />
<sa-chip text="Disabled Deletable" disabled can-delete />
```

## Interactions

```vue
<sa-chip
  text="Toggle me"
  can-toggle
  can-delete
  @toggle="alert('Toggled')"
  @delete="alert('Deleted')"
/>
```
