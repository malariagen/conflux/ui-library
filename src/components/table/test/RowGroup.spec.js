import RowGroup from '@/components/table/rows/RowGroup'

describe('RowGroup', () => {
  test('creates the correct recursive structure', () => {
    const root = new RowGroup(0, 1234, 2, 50)

    expect(root).toEqual({
      id: '0',
      firstRowIndex: 0,
      lastRowIndex: 1234,
      top: 0,
      bottom: 2470,
      height: 2470,
      dataRequested: false,
      visible: false,
      children: [
        {
          id: '0.0',
          firstRowIndex: 0,
          lastRowIndex: 412,
          top: 0,
          bottom: 826,
          height: 826,
          dataRequested: false,
          visible: false,
          children: [
            {
              id: '0.0.0',
              firstRowIndex: 0,
              lastRowIndex: 138,
              top: 0,
              bottom: 278,
              height: 278,
              dataRequested: false,
              visible: false,
              children: null
            },
            {
              id: '0.0.1',
              firstRowIndex: 139,
              lastRowIndex: 276,
              top: 278,
              bottom: 554,
              height: 276,
              dataRequested: false,
              visible: false,
              children: null
            },
            {
              id: '0.0.2',
              firstRowIndex: 277,
              lastRowIndex: 412,
              top: 554,
              bottom: 826,
              height: 272,
              dataRequested: false,
              visible: false,
              children: null
            }
          ]
        },
        {
          id: '0.1',
          firstRowIndex: 413,
          lastRowIndex: 824,
          top: 826,
          bottom: 1650,
          height: 824,
          dataRequested: false,
          visible: false,
          children: [
            {
              id: '0.1.0',
              firstRowIndex: 413,
              lastRowIndex: 551,
              top: 826,
              bottom: 1104,
              height: 278,
              dataRequested: false,
              visible: false,
              children: null
            },
            {
              id: '0.1.1',
              firstRowIndex: 552,
              lastRowIndex: 689,
              top: 1104,
              bottom: 1380,
              height: 276,
              dataRequested: false,
              visible: false,
              children: null
            },
            {
              id: '0.1.2',
              firstRowIndex: 690,
              lastRowIndex: 824,
              top: 1380,
              bottom: 1650,
              height: 270,
              dataRequested: false,
              visible: false,
              children: null
            }
          ]
        },
        {
          id: '0.2',
          firstRowIndex: 825,
          lastRowIndex: 1234,
          top: 1650,
          bottom: 2470,
          height: 820,
          dataRequested: false,
          visible: false,
          children: [
            {
              id: '0.2.0',
              firstRowIndex: 825,
              lastRowIndex: 962,
              top: 1650,
              bottom: 1926,
              height: 276,
              dataRequested: false,
              visible: false,
              children: null
            },
            {
              id: '0.2.1',
              firstRowIndex: 963,
              lastRowIndex: 1099,
              top: 1926,
              bottom: 2200,
              height: 274,
              dataRequested: false,
              visible: false,
              children: null
            },
            {
              id: '0.2.2',
              firstRowIndex: 1100,
              lastRowIndex: 1234,
              top: 2200,
              bottom: 2470,
              height: 270,
              dataRequested: false,
              visible: false,
              children: null
            }
          ]
        }
      ]
    })

    expect(root.toString()).toEqual('0 N [0 - 1234] [0px - 2470px]')
  })

  test('updateVisibility() makes the correct groups visible for the viewport', () => {
    const root = new RowGroup(0, 1234, 2, 50)
    const changed = root.updateVisibility(700, 200)

    // note: toLines outputs children of visible groups only
    expect(root.toLines()).toEqual([
      '0 Y [0 - 1234] [0px - 2470px]',
      ' 0.0 Y [0 - 412] [0px - 826px]',
      '  0.0.0 N [0 - 138] [0px - 278px]',
      '  0.0.1 N [139 - 276] [278px - 554px]',
      '  0.0.2 Y [277 - 412] [554px - 826px]',
      ' 0.1 Y [413 - 824] [826px - 1650px]',
      '  0.1.0 Y [413 - 551] [826px - 1104px]',
      '  0.1.1 N [552 - 689] [1104px - 1380px]',
      '  0.1.2 N [690 - 824] [1380px - 1650px]',
      ' 0.2 N [825 - 1234] [1650px - 2470px]'
    ])

    expect(changed).toBe(true)
    expect(root.updateVisibility(700, 200)).toBe(false)
  })

  test('findMissingData() detects missing rows properly', () => {
    const root = new RowGroup(0, 1234, 2, 50)
    root.updateVisibility(700, 200)

    // start with no fetched data
    const missingGroups = root.findMissingData([])
    expect(missingGroups).toEqual([
      {
        from: 277,
        to: 412,
        group: {
          id: '0.0.2',
          firstRowIndex: 277,
          lastRowIndex: 412,
          top: 554,
          bottom: 826,
          height: 272,
          dataRequested: false,
          visible: true,
          children: null
        }
      },
      {
        from: 413,
        to: 551,
        group: {
          id: '0.1.0',
          firstRowIndex: 413,
          lastRowIndex: 551,
          top: 826,
          bottom: 1104,
          height: 278,
          dataRequested: false,
          visible: true,
          children: null
        }
      }
    ])

    // populate some missing data
    const data = Array.from({ length: 1000 })
    for (let i = 277; i <= 500; i++) {
      data[i] = {}
    }
    expect(root.findMissingData(data)).toEqual([
      {
        from: 501,
        to: 551,
        group: {
          id: '0.1.0',
          firstRowIndex: 413,
          lastRowIndex: 551,
          top: 826,
          bottom: 1104,
          height: 278,
          dataRequested: false,
          visible: true,
          children: null
        }
      }
    ])

    // mark all groups as already requested
    missingGroups.forEach((range) => (range.group.dataRequested = true))
    expect(root.findMissingData(data)).toEqual([])
  })
})
