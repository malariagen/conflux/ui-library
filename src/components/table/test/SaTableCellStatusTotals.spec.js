import { mount } from '@vue/test-utils'
import SaTableCellStatusTotals from '@/components/table/cells/SaTableCellStatusTotals'

describe('SaTableCellStatusTotals', () => {
  test('renders with default props', () => {
    const wrapper = mount(SaTableCellStatusTotals)
    const cell = wrapper.find('span')
    expect(cell.text()).toBe('')
    const icon = wrapper.find('.icon')
    expect(icon.exists()).toBe(false)
  })
  test('renders status totals', () => {
    const wrapper = mount(SaTableCellStatusTotals, {
      propsData: {
        statusTotals: [
          { status: 'inprogress', count: 3 },
          { status: 'complete', count: 5 }
        ]
      }
    })
    const inProgress = wrapper.find('.status--inprogress')
    expect(inProgress.text()).toBe('3')
    const complete = wrapper.find('.status--complete')
    expect(complete.text()).toBe('5')
    const icons = wrapper.findAll('.icon')
    expect(icons.length).toBe(2)
  })
})
