import { mount } from '@vue/test-utils'
import SaTableCellEmpty from '@/components/table/cells/SaTableCellEmpty'

describe('SaTableCellEmpty', () => {
  test('renders empty span', () => {
    const wrapper = mount(SaTableCellEmpty, {
      propsData: {
        value: 'Test Value'
      }
    })
    const cell = wrapper.find('span')
    expect(cell.classes()).toContain('table-cell--empty')
    expect(cell.text()).toBe('')
  })
})
