import { mount } from '@vue/test-utils'
import SaTableCellStatus from '@/components/table/cells/SaTableCellStatus'

describe('SaTableCellStatus', () => {
  test('renders with default props', () => {
    const wrapper = mount(SaTableCellStatus)
    const cell = wrapper.find('span')
    expect(cell.text()).toBe('')
    const icon = wrapper.find('.icon')
    expect(icon.exists()).toBe(false)
  })
  test('renders with inprogress status', () => {
    const wrapper = mount(SaTableCellStatus, {
      propsData: {
        status: 'inprogress'
      }
    })
    const cell = wrapper.find('span')
    expect(cell.text()).toBe('Pending')
    const icon = wrapper.find('.icon')
    expect(icon.exists()).toBeTruthy()
  })
  test('renders with complete status', () => {
    const wrapper = mount(SaTableCellStatus, {
      propsData: {
        status: 'complete'
      }
    })
    const cell = wrapper.find('span')
    expect(cell.text()).toBe('Complete')
    const icon = wrapper.find('.icon')
    expect(icon.exists()).toBeTruthy()
  })
})
