import { mount } from '@vue/test-utils'
import SaTableCellDate from '@/components/table/cells/SaTableCellDate'

describe('SaTableCellDate', () => {
  test('renders with default props', () => {
    const wrapper = mount(SaTableCellDate)
    const cell = wrapper.find('span')
    expect(cell.text()).toBe('')
  })
  test('renders date', () => {
    const wrapper = mount(SaTableCellDate, {
      propsData: {
        date: '2020-02-15T12:34:00Z'
      }
    })
    const cell = wrapper.find('span')
    expect(cell.text()).toBe('15 Feb 20')
  })
})
