import { mount } from '@vue/test-utils'
import SaTableCellDisabled from '@/components/table/cells/SaTableCellDisabled'

describe('SaTableCellDisabled', () => {
  test('renders empty span', () => {
    const wrapper = mount(SaTableCellDisabled, {
      propsData: {
        value: 'Test Value'
      }
    })
    const cell = wrapper.find('span')
    expect(cell.classes()).toContain('table-cell--disabled')
    expect(cell.text()).toBe('')
  })
})
