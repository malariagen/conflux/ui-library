import { mount } from '@vue/test-utils'
import SaTableCellId from '@/components/table/cells/SaTableCellId'

describe('SaTableCellId', () => {
  test('renders without type', () => {
    const wrapper = mount(SaTableCellId, {
      propsData: {
        id: '1'
      }
    })
    const a = wrapper.find('span')
    expect(a.text()).toBe('1')
    expect(a.attributes()).toStrictEqual({
      // href: '#1',
      class: 'table-cell table-cell--id'
    })
  })
  test('renders with type', () => {
    const wrapper = mount(SaTableCellId, {
      propsData: {
        entityType: 'study',
        id: '1'
      }
    })
    const a = wrapper.find('span')
    expect(a.text()).toBe('1')
    expect(a.attributes()).toStrictEqual({
      // href: '/study/1',
      class: 'table-cell table-cell--id'
    })
  })
})
