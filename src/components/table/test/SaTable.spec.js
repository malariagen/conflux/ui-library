import { shallowMount, mount } from '@vue/test-utils'
import SaTable from '@/components/table/SaTable'

const MODEL_RESPONSE = {
  fields: [
    {
      id: 'id_group',
      title: 'IDs',
      subfields: [
        { id: 'sub_id', title: 'Sub ID', type: 'ID<Sample>' },
        { id: 'sub_id_2', title: 'Sub ID 2', type: 'ID<Sample>' }
      ]
    },
    {
      id: 'date_group',
      title: 'Dates',
      subfields: [
        { id: 'sub_date', title: 'Sub Date 1', type: 'Date' },
        { id: 'sub_date_2', title: 'Sub Date 2', type: 'Date' }
      ]
    },
    {
      id: 'received',
      title: 'Received',
      started: {
        id: 'received_start'
      },
      done: {
        id: 'received_done'
      },
      type: 'Event'
    },
    {
      id: 'sequenced',
      title: 'Sequenced',
      started: {
        id: 'sequenced_start'
      },
      done: {
        id: 'sequenced_done'
      },
      type: 'Event'
    },
    {
      id: 'status',
      title: 'Status',
      type: 'Status'
    }
  ]
}

describe('SaTable', () => {
  test('renders table with no columns', () => {
    const wrapper = shallowMount(SaTable, {
      propsData: {
        storeName: 'test'
      },
      mocks: {
        $store: {
          dispatch: jest.fn(),
          state: {
            test: {}
          }
        }
      }
    })
    const table = wrapper.find('.sa-table')
    expect(table.exists()).toBe(true)
  })

  test('renders table with columns but no data', async () => {
    const data = {
      rows: null,
      meta: null,
      model: MODEL_RESPONSE
    }
    const wrapper = shallowMount(SaTable, {
      propsData: {
        storeName: 'test'
      },
      mocks: {
        $store: {
          dispatch: jest.fn(),
          state: {
            test: data
          }
        }
      }
    })
    wrapper.vm.updateGroupVisibility()
    expect(wrapper.find('.table-status').text()).toBe('Loading')
    expect(wrapper.vm.firstRow).toBeNull()
    data.rows = []
    data.meta = {
      total_filtered_results: 0
    }
    await wrapper.vm.$nextTick()
    const tableStatuses = wrapper.findAll('.table-status')
    expect(tableStatuses.length).toBe(1)
    expect(tableStatuses.at(0).text()).toBe('No data found')
    const subHeadings = wrapper.findAll('tr').at(1)
    expect(subHeadings.text()).not.toContain('Start Date')
    expect(wrapper.vm.firstRow).toBeNull()

    // ensure scrolling doesn't cause issues
    wrapper.trigger('scroll')
  })

  test('renders table with date columns', () => {
    const wrapper = shallowMount(SaTable, {
      propsData: {
        storeName: 'test'
      },
      mocks: {
        $store: {
          dispatch: jest.fn(),
          state: {
            test: {
              model: MODEL_RESPONSE,
              columnGroup: 'dates'
            }
          }
        }
      }
    })
    const subHeadings = wrapper.findAll('tr').at(1)
    expect(subHeadings.text()).toContain('Start Date')
  })

  test('renders table with columns and data', async () => {
    const data = {
      rows: [{ id: { sub_id: '1', sub_id_2: '1 (2)' } }],
      meta: {
        total_results: 1,
        total_filtered_results: 1,
        summary: {}
      },
      model: MODEL_RESPONSE
    }
    const wrapper = mount(SaTable, {
      propsData: {
        storeName: 'test'
      },
      mocks: {
        $store: {
          dispatch: jest.fn(),
          state: {
            test: data
          }
        }
      }
    })

    await wrapper.vm.$forceUpdate()
    await wrapper.vm.$nextTick()

    expect(wrapper.find('.table-status').exists()).toBe(false)
    // 1 row
    expect(wrapper.findAll('tbody tr').length).toBe(1)
    expect(wrapper.vm.firstRow).toEqual({
      id: { sub_id: '1', sub_id_2: '1 (2)' }
    })
  })

  test('renders table with columns, data and summary', async () => {
    const data = {
      rows: [
        {
          id: { sub_id: '1', sub_id_2: '1 (2)' },
          events: [
            {
              id: 'received',
              status: 'pending',
              started: {
                id: 'received_start',
                value: null
              },
              done: {
                id: 'received_done',
                value: null
              }
            }
          ],
          status: 'ok'
        }
      ],
      meta: {
        total_results: 1,
        total_filtered_results: 1,
        summary: {
          events: [
            {
              id: 'received',
              statuses: [{ status: 'pending', count: 1 }]
            }
          ]
        }
      },
      model: MODEL_RESPONSE,
      columnGroup: 'status'
    }
    const wrapper = mount(SaTable, {
      propsData: {
        storeName: 'test'
      },
      mocks: {
        $store: {
          dispatch: jest.fn(),
          state: {
            test: data
          }
        }
      }
    })

    await wrapper.vm.$forceUpdate()
    await wrapper.vm.$nextTick()

    expect(wrapper.find('.table-status').exists()).toBe(false)
    // 1 row + 1 summary row
    expect(wrapper.findAll('tbody tr').length).toBe(2)
  })

  test('renders special field types and uses custom render functions', async () => {
    const wrapper = mount(SaTable, {
      propsData: {
        storeName: 'test',
        customRenderers: {
          qc_squash: {
            cellType: 'sa-table-cell-simple',
            getProps: (value) => {
              if (value === 'success') {
                value = '✓'
              }
              return { value }
            }
          },
          label: {
            getProps: (value) => {
              return { value: value + value }
            }
          }
        }
      },
      mocks: {
        $store: {
          dispatch: jest.fn(),
          state: {
            test: {
              model: {
                fields: [
                  {
                    id: 'malgen_lab',
                    type: 'Statuses'
                  },
                  {
                    id: 'qc_squash'
                  },
                  {
                    id: 'label'
                  },
                  {
                    id: 'tags',
                    type: 'Array'
                  }
                ]
              },
              rows: [
                {
                  malgen_lab: {
                    statuses: [{ status: 'complete', count: 1 }]
                  },
                  quality_control: {
                    qc_squash: 'success'
                  },
                  tags: ['A', 'B'],
                  label: 'test'
                }
              ]
            }
          }
        }
      }
    })

    await wrapper.vm.$forceUpdate()
    await wrapper.vm.$nextTick()

    const statusTotals = wrapper.find('.table-cell--status-totals')
    expect(statusTotals.exists()).toBe(true)
    const otherCells = wrapper.findAll('.table-cell--simple')
    expect(otherCells.length).toBe(3)
    expect(otherCells.at(0).text()).toBe('✓')
    expect(otherCells.at(1).text()).toBe('testtest')
    expect(otherCells.at(2).text()).toBe('A, B')
  })

  test('loads data on scroll', async () => {
    const data = {
      model: {
        fields: [{ name: 'id', title: 'ID' }]
      },
      rows: [
        null,
        { id: 1 },
        null,
        null,
        { id: 4 },
        null,
        { id: 6 },
        null,
        { id: 8 },
        null
      ],
      meta: {
        total_results: 20,
        total_filtered_results: 10
      }
    }
    const dispatch = jest.fn()
    const wrapper = mount(SaTable, {
      propsData: {
        storeName: 'test',
        minGroupRows: 2,
        fetchDebounce: 1
      },
      mocks: {
        $store: {
          dispatch,
          state: {
            test: data
          }
        }
      }
    })

    await wrapper.vm.$forceUpdate()
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.firstRow).toEqual({ id: 1 })

    // Initial data load
    expect(dispatch).toHaveBeenCalledWith('test/loadRows', {
      offset: 0,
      limit: 100
    })
    dispatch.mockClear()

    expect(wrapper.find('.table-status').exists()).toBe(false)
    // 4 rows + 6 spacers
    expect(wrapper.findAll('tbody .table-row').length).toBe(10)
    expect(wrapper.findAll('tbody .row-spacer').length).toBe(6)
    // Container height is 0, dispatch not called
    const jestContainer = wrapper.find('.sa-table')
    await jestContainer.trigger('scroll')
    await wait(10)

    // groups will be defined as rows 0-4, 5-8 and 9. Each group is responsible
    // for knowing which of its rows need fetching
    expect(dispatch).toHaveBeenCalledWith('test/loadRows', {
      offset: 0,
      limit: 4
    })
    expect(dispatch).toHaveBeenCalledWith('test/loadRows', {
      offset: 5,
      limit: 3
    })
    expect(dispatch).toHaveBeenCalledWith('test/loadRows', {
      offset: 9,
      limit: 1
    })
  })

  test('reloads data and spacer height on update', async () => {
    const data = {
      rows: [{ id: 1 }],
      meta: {
        total_results: 100,
        total_filtered_results: 100
      },
      model: {
        fields: [{ name: 'id', title: 'ID' }]
      }
    }
    const dispatch = jest.fn()
    const wrapper = mount(SaTable, {
      propsData: {
        storeName: 'test',
        minGroupRows: 100,
        fetchDebounce: 0
      },
      mocks: {
        $store: {
          dispatch,
          state: {
            test: data
          }
        }
      }
    })

    expect(wrapper.findAll('tbody .table-row').length).toBe(1)

    // check spacer height updated using stubbed ref
    wrapper.vm.$refs.testRow = {
      $el: {
        getBoundingClientRect: jest.fn(() => ({ height: 100 }))
      }
    }
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$data.spacerHeight).toBe(100)

    // Initial data load
    expect(dispatch).toHaveBeenCalledWith('test/loadRows', {
      offset: 0,
      limit: 100
    })
    dispatch.mockClear()
    data.rows = []

    // Check update refetches rows
    await wrapper.trigger('scroll')
    await wait(10)
    expect(dispatch).toHaveBeenCalledWith('test/loadRows', {
      offset: 0,
      limit: 100
    })

    // check same data isn't refetched
    dispatch.mockClear()
    data.rows = Array.from({ length: 100 }).map((_, i) => ({ id: i }))
    wrapper.vm.loadRows()
    expect(dispatch).not.toHaveBeenCalled()

    wrapper.destroy()
  })

  test('works if container is somehow destroyed', async () => {
    const wrapper = shallowMount(SaTable, {
      propsData: {
        storeName: 'test'
      },
      mocks: {
        $store: {
          dispatch: jest.fn(),
          state: {
            test: {
              rows: [{ id: 1 }],
              meta: {
                total_results: 1,
                total_filtered_results: 1
              },
              model: {
                fields: []
              }
            }
          }
        }
      }
    })

    const table = wrapper.find('.sa-table')
    expect(table.exists()).toBe(true)
    wrapper.vm.$refs.container = null
    await wrapper.vm.$nextTick()
    wrapper.vm.loadNewData()
  })

  test('cellType and cellProps work as expected', () => {
    const wrapper = shallowMount(SaTable, {
      propsData: {
        storeName: 'test'
      },
      mocks: {
        $store: {
          dispatch: jest.fn(),
          state: {
            test: {
              rows: null,
              meta: null,
              model: MODEL_RESPONSE
            }
          }
        }
      }
    })

    const columnDef = { id: 'id_test', type: 'ID<Sample>' }

    // if data is empty, empty cell is rendered
    let row = {}
    expect(wrapper.vm.cellType(columnDef, row)).toBe('sa-table-cell-empty')
    expect(wrapper.vm.cellProps(columnDef, row)).toEqual({ value: null })

    // if data, id cell is rendered
    row = { id_test: 1 }
    expect(wrapper.vm.cellType(columnDef, row)).toBe('sa-table-cell-id')
    expect(wrapper.vm.cellProps(columnDef, row)).toEqual({
      entityType: 'Sample',
      id: 1
    })

    const columnDef2 = { id: 'date_test', type: 'Date' }

    // if data is empty, empty cell is rendered
    row = {}
    expect(wrapper.vm.cellType(columnDef2, row)).toBe('sa-table-cell-empty')
    expect(wrapper.vm.cellProps(columnDef2, row)).toEqual({ value: null })

    // if data, date cell is rendered
    row = { date_test: 'now' }
    expect(wrapper.vm.cellType(columnDef2, row)).toBe('sa-table-cell-date')
    expect(wrapper.vm.cellProps(columnDef2, row)).toEqual({ date: 'now' })

    const columnDef3 = { key: 'date_test', type: 'Disabled' }

    // if type disabled, cell is disabled whether data is provided or not
    row = {}
    expect(wrapper.vm.cellType(columnDef3, row)).toBe('sa-table-cell-disabled')
    expect(wrapper.vm.cellProps(columnDef3, row)).toEqual({ value: null })

    // if data is provided, it is not passed to component
    row = { date_test: 'now' }
    expect(wrapper.vm.cellType(columnDef3, row)).toBe('sa-table-cell-disabled')
    expect(wrapper.vm.cellProps(columnDef3, row)).toEqual({ value: null })
  })

  test('artificial row limit is respected', async () => {
    const rows = Array.from({ length: 1000 })
    rows[0] = { id: 1 }

    const wrapper = shallowMount(SaTable, {
      propsData: {
        storeName: 'test',
        maxRows: 600,
        extraRowsMessage:
          'Too many rows. Please use filters to see remaining :count rows'
      },
      mocks: {
        $store: {
          dispatch: jest.fn(),
          state: {
            test: {
              rows,
              meta: {
                total_results: 1000,
                total_filtered_results: 1000,
                summary: {}
              },
              model: MODEL_RESPONSE
            }
          }
        }
      }
    })

    await wrapper.vm.$forceUpdate()
    await wrapper.vm.$nextTick()

    const extra = wrapper.find('.extra-rows')
    expect(extra.text()).toEqual(
      'Too many rows. Please use filters to see remaining 400 rows'
    )
  })

  test('api errors are handled appropriately', async () => {
    const wrapper = shallowMount(SaTable, {
      propsData: {
        storeName: 'test'
      },
      mocks: {
        $store: {
          dispatch: jest.fn().mockRejectedValue(new Error('Failed API')),
          state: {
            test: {
              rows: null,
              meta: null,
              model: MODEL_RESPONSE
            }
          }
        }
      }
    })

    await wrapper.vm.$forceUpdate()
    await wait(1000)
    await wrapper.vm.$nextTick()

    expect(wrapper.emitted()).toEqual({
      apierror: [
        ['Failed to load rows 0 - 99: Failed API'],
        ['Failed to load rows 0 - 99: Failed API'],
        ['Failed to load rows 0 - 99: Failed API'],
        ['Failed to load rows 0 - 99: Failed API'],
        ['Failed to load rows 0 - 99: Failed API'],
        ['Failed to load rows 0 - 99: Failed API'],
        ['Failed to load rows 0 - 99: Failed API'],
        ['Failed to load rows 0 - 99: Failed API'],
        ['Failed to load rows 0 - 99: Failed API'],
        ['Failed to load rows 0 - 99: Failed API'],
        ['Failed to load initial rows']
      ]
    })
  })

  test('re-triggers initial data load when filter changes', async () => {
    const data = {
      rows: null,
      meta: null,
      model: MODEL_RESPONSE,
      filters: {}
    }
    const dispatch = jest.fn()
    const wrapper = shallowMount(SaTable, {
      propsData: {
        storeName: 'test'
      },
      mocks: {
        $store: {
          dispatch,
          state: {
            test: data
          }
        }
      }
    })

    expect(dispatch).toHaveBeenCalledWith('test/loadRows', {
      offset: 0,
      limit: 100
    })
    dispatch.mockClear()
    data.filters = { a: 'b' }
    await wrapper.vm.$nextTick()
    expect(dispatch).toHaveBeenCalledWith('test/loadRows', {
      offset: 0,
      limit: 100
    })
  })
})

const wait = (ms) => new Promise((resolve) => setTimeout(resolve, ms))
