import { mount } from '@vue/test-utils'
import SaTableCellSimple from '@/components/table/cells/SaTableCellSimple'

describe('SaTableCellSimple', () => {
  test('renders text value', () => {
    const wrapper = mount(SaTableCellSimple, {
      propsData: {
        value: 'Test Value'
      }
    })
    const cell = wrapper.find('span')
    expect(cell.text()).toBe('Test Value')
  })
  test('renders html value', () => {
    const wrapper = mount(SaTableCellSimple, {
      propsData: {
        value: '<p>Test HTML</p>'
      }
    })
    const value = wrapper.find('p')
    expect(value.text()).toBe('Test HTML')
  })
})
