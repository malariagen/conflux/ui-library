import { mount } from '@vue/test-utils'
import RowGroup from '@/components/table/rows/RowGroup'
import SaTableRowGroup from '@/components/table/rows/SaTableRowGroup'

const Table = {
  components: { SaTableRowGroup },
  template: `
    <table>
      <SaTableRowGroup v-bind="$attrs" />
    </table>
  `
}

describe('SaTableRowGroup', () => {
  test('creates the correct set of <tbody> from the recursive structure', () => {
    const root = new RowGroup(0, 999, 2, 40)
    root.updateVisibility(500, 200)

    const wrapper = mount(Table, {
      propsData: {
        group: root,
        columns: [{ name: 'id' }],
        allRows: Array.from({ length: 1000 }),
        cellType: () => 'sa-table-cell-simple',
        cellProps: () => ({}),
        spacerHeight: 20
      }
    })

    const groups = wrapper.findAll('tbody')

    const expectedGroups = [
      {
        groupId: '0.0.0',
        type: 'table-filler',
        rows: 1,
        height: 226
      },
      {
        groupId: '0.0.1',
        type: 'table-filler',
        rows: 1,
        height: 224
      },
      {
        groupId: '0.0.2',
        type: 'table-body',
        rows: 110
      },
      {
        groupId: '0.1.0',
        type: 'table-body',
        rows: 113
      },
      {
        groupId: '0.1.1',
        type: 'table-filler',
        rows: 1,
        height: 224
      },
      {
        groupId: '0.1.2',
        type: 'table-filler',
        rows: 1,
        height: 218
      },
      {
        groupId: '0.2',
        type: 'table-filler',
        rows: 1,
        height: 662
      }
    ]

    expect(groups.length).toEqual(expectedGroups.length)

    expectedGroups.forEach((group, index) => {
      const dom = groups.at(index)
      expect(dom.attributes()['data-group-id']).toEqual(group.groupId)
      expect(dom.classes()).toEqual([group.type])
      if (group.rows) {
        expect(dom.findAll('tr').length).toEqual(group.rows)
      }
      if (group.height) {
        expect(dom.find('td').attributes().style).toEqual(
          `height: ${group.height}px;`
        )
      }
    })
  })
})
