Use the `table` component to display large amounts of data from a server-side
source. Best used in combination with the `filters` component.

The component uses lazy loading and virtual scrolling to handle millions of rows
while staying performant. There are, however, some practical limits on the
length of a page that browsers are able to handle. This means by default we only
show the first 50,000 rows in the table. This value can be changed with the
`maxRows` prop.

## Expected Data

The table component builds columns and rows based on the data that is passed
into it. This expects a specific data structure to work correctly.

First there should be a 'model', which loads the columns (and filters). Then the
'data' which fills the rows with data.

## Model

Example model:

```js static
{
  fields: [
    {
      id: 'standard_field',
      title: 'Standard Field'
    },
    {
      id: 'id_field',
      title: 'ID Field'
      type: 'ID'
    },
    {
      id: 'event_field',
      title: 'Event Field'
      type: 'Event'
      started: {
        id: 'event_field_start'
      },
      done: {
        id: 'event_field_end'
      }
    },
    {
      id: 'subfield',
      title: 'Subfield',
      subfields: [
        {
          id: 'subfield_1',
          title: 'Subfield 1'
        },
        {
          id: 'subfield_2',
          title: 'Subfield 2'
        }
      ]
    }
  ],
  filters: []
}
```

The `fields` array will tell the table which columns to expect and match these
with the data.

The `filters` array, when coupled with the filter component, will set the
filters and their options (see the filter component docs for more details).

### Fields

Properties used for fields in the `fields` array:

| Property         | Type                          | Description                                                                                                                                                                                                                                                                                   |
| ---------------- | ----------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `id`             | String (required)             | The `id` field should correspond to the relevant key/id in the data. This is how the table identifies which data to add to each column.                                                                                                                                                       |
| `title`          | String (required)             | The `title` field will set the heading for the column.                                                                                                                                                                                                                                        |
| `type`           | String                        | You can set the data type for the column from preset options (see Types table below). It is recommended to set the type relevant to the data. The table uses `type` to display special column/cell types. If not set, this will display a simple table cell with the value found in the data. |
| `subfields`      | Array                         | The `subfields` array allows you to split a column. You can add field objects inside this array that must each include an `id` and `title`.                                                                                                                                                   |
| `started`/`done` | Objects (required for Events) | If `type` is set to `Event`, you must also include a `started` and `done` object. Each must contain an `id` that corresponds to the `id` in the data (see example).                                                                                                                           |

### Types

Options for the field `type` (see the Data section for expected data examples):

| Type       | Data Expected                          | Description                                                                                                                                                                                                                                                                                                                           |
| ---------- | -------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `ID`       | String                                 | Use for ID fields.                                                                                                                                                                                                                                                                                                                    |
| `Date`     | Date string                            | Use for a single date. This will convert a date string to the correct format. If the data is an 'event' object (includes `started`, `done` and `status`), you may want to use the `Event` type. If you use type `Date` on an event, make sure to include the `id` of one of the `started` or `done` dates, rather than the parent id. |
| `Event`    | Event object                           | If the data you want to include is an event, you may want to use an `Event` type. This will allow the table to switch between a single column Status view, and a split-column Start/End Dates view (if enabled).                                                                                                                      |
| `Status`   | Status string                          | Outputs the field with a related status icon.                                                                                                                                                                                                                                                                                         |
| `Statuses` | Object containing an array of statuses | Use for fields with multiple statuses. This will output an icon and count for each status.                                                                                                                                                                                                                                            |
| `Boolean`  | Boolean                                | For boolean (true/false) fields.                                                                                                                                                                                                                                                                                                      |
| `Number`   | Number                                 | For fields that contain a number.                                                                                                                                                                                                                                                                                                     |
| `Array`    | Array of strings                       | For fields that contain multiple strings in an array.                                                                                                                                                                                                                                                                                 |
| `Disabled` | Any data                               | Use a type of `Disabled` to hide all data for cells in this column.                                                                                                                                                                                                                                                                   |

## Data

Example data:

```js static
{
  meta: {
    limit: 100,
    offset: 0,
    request_id: '',
    summary: {
      ids: [
        {
          id: 'id_field',
          value: { total: 0 }
        }
      ],
      events: [
        {
          id: 'event_field',
          // use 'started' and 'done' to show totals in date view
          started: {
            id: 'event_field_start',
            value: { total: 0 }
          },
          done: {
            id: 'event_field_end',
            value: { total: 0 }
          },
          // use 'statuses' to show totals in status view
          statuses: [
            { status: 'pending', count: 0 },
            { status: 'complete', count: 0 }
          ]
        }
      ],
      status_field: {
        statuses: [
          { status: 'pending', count: 0 },
          { status: 'complete', count: 0 }
        ]
      },
      // for subfields use subfield key
      subfield_1: { total: 0 },
      subfield_2: { total: 0 },
    },
    total_filtered_results: 1000,
    total_results: 1000
  },
  rows: [
    {
      ids: [
        {
          id: 'id_field',
          value: 'ABC-1234'
        }
      ],
      events: [
        {
          id: 'event_field',
          started: {
            id: 'event_field_start',
            value: '2000-01-01T00:00:00.000Z'
          },
          done: {
            id: 'event_field_end',
            value: '2000-01-01T00:00:00.000Z'
          },
          status: 'complete'
        },
      ],
      date_field: '2000-01-01T00:00:00.000Z',
      status_field: 'complete',
      statuses_field: {
        statuses: [
          { status: 'pending', count: 1234 },
          { status: 'complete', count: 1234 }
        ]
      },
      boolean_field: true,
      number_field: 1234,
      array_field: ['One', 'Two', 'Three', 'Four'],
      disabled_field: 'ABCXYZ',
      subfield: {
        subfield_1: 'ABC',
        subfield_2: 'XYZ'
      }
    }
  ]
}
```

The `meta` object contains useful information that is required for the table to
work. There are also optional features such as the `summary`, which generates a
summary row in the table.

The `rows` array contains the data used in the rows of the table. The model
matches field IDs with the IDs found in each row to determine which items are
displayed.

### Meta

Properties used in the `meta` object:

| Property                 | Type   | Description                                                                                                                                                              |
| ------------------------ | ------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `limit`                  | Number | The number of results (`rows`) returned in the API response.                                                                                                             |
| `offset`                 | Number | Pagination of results returned. Used alongside `limit` to calculate rows to load in infinite scrolling.                                                                  |
| `request_id`             | Number | An ID for each request made to the API.                                                                                                                                  |
| `summary`                | Object | This object is used to display a summary row in the table. The summary displays total counts for given fields, identified by the field key/id in the data (see example). |
| `total_filtered_results` | Number | Returns total number of results after filters have been applied.                                                                                                         |
| `total_results`          | Number | Returns absolute total number of results.                                                                                                                                |

### Rows

Most data types in the rows will be simple key value pairs. The `id` set in the
model in these cases will relate to the keys in the `rows`.

For `ids` and `events` fields, we may need more complex data, so these are
grouped in arrays and use specific `id` properties. The `id` set in the model in
these cases will relate to the `id` value in the data.

#### IDs

Properties expected for IDs in the `ids` array:

| Property | Type              | Description                                      |
| -------- | ----------------- | ------------------------------------------------ |
| `id`     | String (required) | This is the identifier for the current ID field. |
| `value`  | String (required) | This is the value for the field.                 |

#### Events

Properties expected for events in the `events` array:

| Property                 | Type              | Description                                                                                                                                                                        |
| ------------------------ | ----------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `id`                     | String (required) | This is the identifier for the event as a whole.                                                                                                                                   |
| `started`/`done`         | Object (required) | The `started` and `done` objects have the same properties. They represent the beginning and end of the event and will be shown in `Event` date view or when used as a `Date` type. |
| `started`/`done`.`id`    | String (required) | This is the identifier for the started/done aspect of the event.                                                                                                                   |
| `started`/`done`.`value` | String (required) | Expects a date string for the time the started/done aspect of the event occurred.                                                                                                  |
| `status`                 | String (required) | The status of the event. Will be shown in `Event` status view.                                                                                                                     |

## Custom renderers

You can customise the way the data in each cell is displayed by providing custom
render functions. The `custom-renderers` prop expects an object where the keys
are the field IDs and the values are the renderers.

```js static
{
  field_id: {
    getProps: (value, row) => ({
      value: `<strong>${value}</strong>`
    })
  }
}
```

A renderer is an object with a `getProps` function that receives the cell value
as the first parameter, the row of data as the second. It should return the new
cell props, or `null` to fall back to the default renderer.

See below for a full code example.

## Example

```vue
<template>
  <section>
    <sa-toolbar>
      <template v-slot:left>
        <sa-filter :store-name="storeName" :has-query-params="false" />
      </template>
      <template v-slot:right>
        <sa-toggle
          label="View Data"
          size="small"
          :options="columnGroups"
          :initial-value="columnGroup"
          @change="(group) => setColumnGroup(group.value)"
        />
        <sa-button
          text="Export"
          size="small"
          :disabled="exporting"
          :icon="exporting ? 'loader' : null"
          :spin="exporting"
          @click="exportData()"
        />
      </template>
    </sa-toolbar>
    <sa-table
      :custom-renderers="customRenderers"
      :show-summary-groups="showSummaryGroups"
      :store-name="storeName"
      @apierror="onTableError"
    />
    <sa-toaster :toasts="toasts" @closedtoast="removeToast" />
  </section>
</template>

<script>
/*
In an app, you would get these imports from the main package:
import { tableStore, COLUMN_GROUPS } from '@sanger-surveillance-operations/ui-library'
*/
import tableStore from '@/mixins/tableStore'
import { COLUMN_GROUPS } from '@/data/constants'

export default {
  // this mixin provides conveniences for accessing the store
  mixins: [tableStore],
  props: {
    // expected by the tableStore mixin
    storeName: {
      type: String,
      default: 'samples'
    }
  },
  data() {
    return {
      // use column groups when there are too many columns to show on one screen
      columnGroups: [
        { label: 'Status', value: COLUMN_GROUPS.Status },
        { label: 'Dates', value: COLUMN_GROUPS.Dates }
      ],
      // show the table summary for these column groups
      showSummaryGroups: [COLUMN_GROUPS.Dates],
      // state of export button
      exporting: false,
      // currently displayed toast messages
      toasts: []
    }
  },
  computed: {
    tableData() {
      return this.tableStore.state
    },
    columnGroup() {
      return this.tableData.columnGroup
    },
    customRenderers() {
      return {
        // display `boolean_field` values in italic if the `number_field`
        // for that row has a value less than 5000
        boolean_field: {
          getProps: (value, row) => {
            console.log(`row`, row)
            return {
              value: row.number_field < 5000 ? `<em>${value}</em>` : value
            }
          }
        }
      }
    }
  },
  // trigger the initial load
  created() {
    if (!this.tableStore.model) {
      this.tableStore.dispatch('loadModel')
    }
  },
  methods: {
    // handle API errors from table
    onTableError(err) {
      this.addToast(err, 'danger')
    },
    // toast message helper
    addToast(message, type, expire = 10000) {
      const toast = { message, type }
      this.toasts.push(toast)
      setTimeout(() => this.removeToast(toast), expire)
    },
    // toast message helper
    removeToast(toast) {
      const index = this.toasts.indexOf(toast)
      if (index > -1) {
        this.toasts.splice(index, 1)
      }
    },
    setColumnGroup(value) {
      this.tableStore.dispatch('setColumnGroup', value)
    },
    // ask the store to export the current table data
    exportData() {
      this.exporting = true
      this.tableStore
        .dispatch('exportData')
        .then((csv) => {
          console.log(csv)
          window.alert('See console for csv output')
        })
        .finally(() => {
          this.exporting = false
        })
    }
  }
}
</script>
```
