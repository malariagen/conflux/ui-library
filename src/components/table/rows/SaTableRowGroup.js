import SaTableFiller from './SaTableFiller'
import SaTableBody from './SaTableBody'

/**
 * Functional component, which generates tbody sections to represent a large table
 * with a relatively small number of DOM elements.
 * This component needs to be functional, to allow multiple root (tbody) elements
 */
export default {
  name: 'SaTableRowGroup',
  functional: true,
  render(createElement, { props }) {
    const { group } = props

    if (!group.visible) {
      // render a filler row when not visible (to take up the correct height)
      return createElement(SaTableFiller, {
        props: { ...props, height: group.height, groupId: group.id }
      })
    } else if (group.children) {
      // recurse down the tree
      return group.children.map((child) => {
        return createElement('sa-table-row-group', {
          props: { ...props, group: child }
        })
      })
    } else {
      // render the actual rows
      return createElement(SaTableBody, { props })
    }
  }
}
