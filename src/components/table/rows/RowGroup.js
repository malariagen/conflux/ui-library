/**
 * Class to represent a (sub)section of table rows. Each group is subdivided recursively
 * so that each leaf group contains at least minRows
 * A RowGroup is used as the model behind an sa-table-row-group, and is responsible for
 * maintaining the visibility of the group, and for fetching any missing data
 * If a group is not on screen, it is displayed as a single 'filler' row with a fixed
 * height. Otherwise it recursively renders each of its children, until the leaves are
 * reached, which are displayed as actual table rows
 */
export default class RowGroup {
  /**
   * Creates a recursive set of row groups for the given row indexes, where each group
   * can have either 0 or 3 children, depending on the number of rows at each level,
   * and the minimum number of rows
   * @param {Number} firstRowIndex
   * @param {Number} lastRowIndex
   * @param {Number} rowHeight The fixed height of each row
   * @param {Number} minRows The minimum number of rows per group
   * @param {Number} offset Used if there is a header (defaults to zero)
   * @param {String} id A unique identifier within the tree
   */
  constructor(
    firstRowIndex,
    lastRowIndex,
    rowHeight,
    minRows,
    offset = 0,
    id = '0'
  ) {
    this.id = id
    this.firstRowIndex = firstRowIndex
    this.lastRowIndex = lastRowIndex
    this.visible = false
    this.children = null
    this.top = offset + this.firstRowIndex * rowHeight
    this.bottom = offset + (this.lastRowIndex + 1) * rowHeight
    this.height = (this.lastRowIndex - this.firstRowIndex + 1) * rowHeight
    this.dataRequested = false

    // generate children if there are enough rows to share amongst them
    const rowCount = lastRowIndex - firstRowIndex + 1
    if (rowCount > minRows * 3) {
      const childCount = Math.ceil(rowCount / 3)
      this.children = [0, 1, 2].map(
        (slice) =>
          new RowGroup(
            firstRowIndex + slice * childCount + (slice > 0 ? 1 : 0),
            Math.min(
              firstRowIndex + (slice + 1) * childCount,
              this.lastRowIndex
            ),
            rowHeight,
            minRows,
            offset,
            id + '.' + slice
          )
      )
    }
  }

  toString() {
    return this.toLines().join('\n')
  }

  // handy for debugging
  toLines(prefix = '') {
    const lines = []
    const rows = `[${this.firstRowIndex} - ${this.lastRowIndex}]`
    const pixels = `[${this.top}px - ${this.bottom}px]`
    const visible = this.visible ? 'Y' : 'N'
    lines.push(`${prefix}${this.id} ${visible} ${rows} ${pixels}`)
    if (this.visible) {
      if (this.children) {
        this.children.forEach((child) => {
          child.toLines(prefix + ' ').forEach((line) => lines.push(line))
        })
      }
    }
    return lines
  }

  updateVisibility(scrollTop, containerHeight) {
    const scrollBottom = scrollTop + containerHeight
    const visible = scrollTop <= this.bottom && scrollBottom >= this.top
    let changed = visible !== this.visible
    if (changed) {
      this.visible = visible
    }
    if (visible && this.children) {
      this.children.forEach((child) => {
        changed |= child.updateVisibility(scrollTop, containerHeight)
      })
    }
    return Boolean(changed)
  }

  findMissingData(allRows) {
    const missingRanges = []
    if (this.visible) {
      if (this.children) {
        this.children.forEach((child) => {
          child.findMissingData(allRows).forEach((range) => {
            missingRanges.push(range)
          })
        })
      } else if (!this.dataRequested) {
        const missing = []
        for (let i = this.firstRowIndex; i <= this.lastRowIndex; i++) {
          if (!allRows[i]) {
            missing.push(i)
          }
        }
        if (missing.length > 0) {
          missingRanges.push({
            from: missing[0],
            to: missing[missing.length - 1],
            group: this
          })
        }
      }
    }
    return missingRanges
  }
}
