import { mount } from '@vue/test-utils'
import SaCard from '@/components/card/SaCard'

describe('SaCard', () => {
  test('it works with minimal props', () => {
    const wrapper = mount(SaCard)
    expect(wrapper).toBeTruthy()
    expect(wrapper.find('.title').exists()).toBeFalsy()
    expect(wrapper.find('.subtitle').exists()).toBeFalsy()
    expect(wrapper.find('.image').exists()).toBeFalsy()
    expect(wrapper.find('.body').exists()).toBeFalsy()
    expect(wrapper.find('.footer').exists()).toBeFalsy()
  })

  test('it displays the slots and props', () => {
    const wrapper = mount(SaCard, {
      propsData: {
        subtitle: 'with a subtitle',
        image:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH5AgMDzoJbI4mrQAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAADElEQVQI12NgYWEBAAAcAA2dNkFmAAAAAElFTkSuQmCC',
        imageAlt: 'alt text',
        title: 'ignored' // component uses slot over prop
      },
      slots: {
        body: '<p>Some body text</p>',
        footer: '<footer>A footer</footer>',
        title: '<b>a card</b>'
      }
    })
    expect(wrapper.find('.image img').attributes('alt')).toBe('alt text')
    expect(wrapper.find('.title').text()).toBe('a card')
    expect(wrapper.find('.subtitle').text()).toBe('with a subtitle')
    expect(wrapper.find('.body').text()).toBe('Some body text')
    expect(wrapper.find('.footer').text()).toBe('A footer')
  })
})
