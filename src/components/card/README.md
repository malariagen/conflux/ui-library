Cards can display content and actions.

```vue
<sa-card title="Card title" subtitle="Card subtitle">
  <template v-slot:body>
    Card content
  </template>
  <template v-slot:footer>
    Card footer
  </template>
</sa-card>
```

## More examples

```vue
<sa-grid>
  <sa-card title="Card 1" subtitle="Created by me">
    <template v-slot:body>
      <p>Short description</p>
      <sa-stats title="Samples" :stats="{ Total: 1000, Matched: 1000 }" />
    </template>
    <template v-slot:footer>
      <div style="display: flex; justify-content: space-between;">
        <sa-button text="Cancel" type="danger" size="small" />
        <sa-button text="OK" size="small" />
      </div>
    </template>
  </sa-card>
  <sa-card>
    <template v-slot:title>
      <a href="#">Card with title slot</a>
    </template>
    <template v-slot:body>
      <p>
        Medium description. Duis mollis, est non commodo luctus, nisi erat 
        porttitor.
      </p>
      <sa-stats
        title="Samples"
        :stats="{ Total: 1478, Matched: 1178, Missing: 286 }"
      />
    </template>
    <template v-slot:footer>
      <sa-button text="OK" size="small" />
    </template>
  </sa-card>
  <sa-card title="No footer">
    <template v-slot:body>
      <p>
        Long description. Etiam porta sem malesuada magna mollis euismod.
        Duis mollis, est non commodo luctus, nisi erat porttitor ligula,
        eget lacinia odio sem nec elit.
      </p>
    </template>
  </sa-card>
  <sa-card
    title="Image header card"
    image="data:image/gif;base64,R0lGODlhLgFDAPQcABUAjR4AkicPmDEfnjstpEQ6qk9HsFhUtmJgvGtswnR5x3+GzYiS0/+VAP6aAJKf2PWhIO+lOuqoTuSqX96ub9mxf9Ozjs62nse6rsK8vLu/y7DF6QAAAAAAAAAAAAAAACH5BAUAABwALAAAAAAuAUMAAAX/4CaOZGmeaKqubOu+cCzPdG3feK7vfO//wKBwSCwaj8ikcslsOp/QqHRKrVqv2Kx2y+16v+CweEwum8/otHrNbrvf8Lh8Tq/b7/i8fs/v+98MgYKDhCqEh4NAiIJTi4FKGRQQDg2VlhMXLhibnJ2en6AYIxmhnCqlojkIAKytrq8MKAyvtK4IPrO1AARTuqxIkpbCww4SGqfEycrKI5PLDinBzKq+tLcnq9W2uL68UtVGFZTPxBQpGOTpxM3k5ifSyTrZ2qzeJgT0rdc8ubX2UOCIOFO37gQ6ggTZkUMBryCOeflQ5NPHTde/JwGDZBiH0CGJgx0Xihi4zF2JhsPk/01k9cBEv3z7drx8ddFJRiAcQ6YsAVLntA0klx07+UzlygQmEqwEEFPHTFc1m9z0kdPnJZ5WhY5UZ4yo1odLC9xb2jTH01ZRmRBYy7ZtkAlZ432MKxcowQxef96AGLFEALIV/fmhW24uYWEK1eWtu3cpgJYjHjgu6/LQirP1Wlh2+ggHIhYS0nUVYaGqx56HE6dLRbooNcAjlMJOiq8aAaQuu8kyMMC2gQUu2wpXIGJBb1vChe+u3Y34idXPn1UYJXJG0OobUCJ+vVLsiAKTx84urruE7KWxIvu6ZaAW31fBw5MQ9wyvweqoPca4Ln2EdktGLUXCX+NhRs8AJBh4Ef9z8m1gIAIH6PKeK+Y5RpFqhaUQgWsb5LeTdSH5x2FjjqnXoGQWomWiYN+l2MqKtDD4yoQvxuYiK87Z9dU5pVA34n4hufNfJQEeRV6DMloII0023sjUkZPd5OQvHWJXg4fDTKLlllxCYN9WIWb3Yw001sNiezFWcw2K3SQ5I5Qxttjmcb7AeVRG51nkpistYQkgDn7qxJqOHZkzZANFWsOiAO6pKUKZuD1anoIjlGcnfA6mWCYActaSngh00hLLoRMwRMGpqKaKaiqBhjQof6sdmigsuoigC5vuScrigBZdqmKmt1YYrIFqZkSgNSU8KGZJKBDkTqsdveqTBLJyV8v/Boxao0Ctjjq4yAnHMgnsro4k5UssxOqjQCAQbhrZt8mut6wyJpXgbGuHNSBtSctsuKMN7qI5o8DbdOvCA+D1Om6cMGx77sK7+lVnw6G+SWqzXOF72L7MmKaYtbRArA1xBp/ALm/hskipIRCuRQ+62qQwlUsJtEzPLRdHJ5rGhHEcD31hgjWxgNjKm+wBFefjzcrmJewYzEaDO3HT2QJ2gZUY0sszXT4X5DHWNLhbtNK2Ri1yWL5mRgLBFkIdrERT6+riLa1ivHOV5HSp9yRfEirX1UGTWOsGEcJUtoSduri0pbi66Pa1MsedtHzt6Fw53svUACuRYCI0K4Vne3p4/66ET7lL2qeH/rTqCEY++NhOXvP1nzxxApfWmOsFZOYibNTR5zXCbqzRMBG35+IK7wkVAg+U+bi4cA++aT0HAGc00LjbncyzYLuwOQQkhOY5yJgKr7D52xSO+NoKr2wbcE2KzrTUg09+YaXra3C3CeLj3ioqPfKbfmanHzLh6Wb4Q5zyDmCClP2KUsQy17AsRT/IVeNTqNuHv9rBqkNVgnv5+mDnGDOvdACPSqprBWTQRxHlwW9JUEEdLxwWMVA9bH4NHFrc5IasreULhCF0x/dMQMDtCO11LATd6JClvAVNSjcLuCAJ8iQq1qkgICVr3JtEFEIR5u4wQhxTaT52RP/I8bCHS3yT8naxLvW1D4ojC8QaH2NF11nwfQtgGxpHCEYf0iWMvLMXGQVnxhTmKIlPoqLiZJhGx9UxepCzX4N6V8Qg+TEugNTdJT9EyJDxComIvIbpUrcyPaboeTG0oydpGDsT+O6Hm7RKJkmYtQKGbSr2a10CSTe9iTCykRa6BQ4l9jpJduc+groOEGEpQE4aZky33KEbt/FJ0pXuZs57pBZvBcfzVdCTNryZJKNRqGYus4/NNCL/uicDscEQR8S0ZgoHABxWMgmHpryQA180zGqC84zictoe13m5EnoRWpZMJ+0sR0syNA9pa4HQCnFhswMwrwoMaFlEXwioYGw8CRN/CKlIR0rSkpr0pChNqUpXytKWuvSlMI2pTGdK05ra9KY4zalOd8rTnvr0p0ANqlCHStSiGvWoSwgBADs="
    image-alt="MalariaGen"
  >
    <template v-slot:body>
      This card has a header image and a title but no footer.
    </template>
  </sa-card>
</sa-grid>
```
