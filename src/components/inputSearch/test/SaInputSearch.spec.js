import { mount } from '@vue/test-utils'
import SaInputSearch from '@/components/inputSearch/SaInputSearch'

describe('SaInputSearch', () => {
  test('renders with default props', () => {
    const wrapper = mount(SaInputSearch)
    const component = wrapper.find('.input-search')
    expect(component.classes()).toContain('regular')
  })

  test('emits event when input changes', async () => {
    const wrapper = mount(SaInputSearch, {
      propsData: {
        timeout: 1
      }
    })
    const input = wrapper.find('input')
    await input.setValue('x')
    await input.setValue('y')

    const component = wrapper.find('.input-search')
    expect(component.classes()).toContain('active')

    // wait for debounced event to trigger
    await new Promise((resolve) => setTimeout(resolve, 10))
    expect(wrapper.emitted().change).toEqual([['y']])
  })

  test('focuses input when container is clicked', async () => {
    const elem = document.createElement('div')
    document.body.appendChild(elem)
    const wrapper = mount(SaInputSearch, { attachTo: elem }) // attachToDocument is deprecated: https://vue-test-utils.vuejs.org/api/options.html#attachtodocument
    const input = wrapper.find('input')
    await wrapper.vm.$nextTick()

    expect(input.element).not.toEqual(document.activeElement)

    const component = wrapper.find('.input-search')
    component.trigger('click')
    await wrapper.vm.$nextTick()

    expect(input.element).toEqual(document.activeElement)
  })

  test("clicking the 'clear' button clears the input", async () => {
    const wrapper = mount(SaInputSearch)
    const input = wrapper.find('input')
    await input.setValue('x')
    expect(input.element.value).toEqual('x')

    const clear = wrapper.find('.clear')
    await clear.trigger('click')

    expect(input.element.value).toEqual('')
  })
})
