import { mount } from '@vue/test-utils'
import SaSelect from '@/components/select/SaSelect'

describe('SaSelect', () => {
  test('renders with default props', () => {
    const wrapper = mount(SaSelect)
    const select = wrapper.find('.select')
    expect(select.classes()).toContain('regular')
    expect(select.text()).toEqual('Select')
  })
  test('opens and closes dropdown', async () => {
    const wrapper = mount(SaSelect)
    const select = wrapper.find('.select')
    const dropdown = wrapper.find('.dropdown')
    expect(dropdown.element).not.toBeVisible()
    // open with button
    await select.trigger('click')
    expect(dropdown.element).toBeVisible()
    // close with button
    await select.trigger('click')
    expect(dropdown.element).not.toBeVisible()
    // open with button
    await select.trigger('click')
    expect(dropdown.element).toBeVisible()
    // close on focusout
    await dropdown.trigger('focusout')
    expect(dropdown.element).not.toBeVisible()
  })
  test('display props are respected', async () => {
    const wrapper = mount(SaSelect, {
      propsData: {
        fullWidth: true,
        align: 'right',
        size: 'small'
      }
    })

    const select = wrapper.find('.select')
    expect(select.classes()).toContain('full-width')
    expect(select.classes()).toContain('small')

    await select.trigger('click')
    const dropdown = wrapper.find('.dropdown')
    expect(dropdown.classes()).toContain('right')
  })
  test('focus is controllable with keyboard', async () => {
    const elem = document.createElement('div')
    document.body.appendChild(elem)
    const wrapper = mount(SaSelect, {
      attachTo: elem,
      propsData: {
        options: [
          { label: 'Option 1', value: 1 },
          { label: 'Option 2', value: 2 }
        ]
      }
    })

    const select = wrapper.find('.select')
    await select.trigger('click')

    await wait(10)

    const dropdown = wrapper.find('.dropdown')
    const options = dropdown.findAll('.option')
    expect(options.length).toBe(2)

    // ensure the dropdown is focused
    expect(document.activeElement).toBe(dropdown.element)

    // move up, to select the last option
    await dropdown.trigger('keydown.up')
    expect(document.activeElement).toBe(options.at(1).element)

    // close dropdown
    await select.trigger('click')
    await wait(10)
    expect(document.activeElement).toBe(select.element)

    // reopen dropdown
    await select.trigger('click')
    await wait(10)

    // ensure the dropdown is focused
    expect(document.activeElement).toBe(dropdown.element)

    // select the first option with down key
    await dropdown.trigger('keydown.down')
    expect(document.activeElement).toBe(options.at(0).element)

    // move down again
    await options.at(0).trigger('keydown.down')
    expect(document.activeElement).toBe(options.at(1).element)

    // try to move down (and fail)
    await options.at(1).trigger('keydown.down')
    expect(document.activeElement).toBe(options.at(1).element)

    // move back up again
    await options.at(1).trigger('keydown.up')
    expect(document.activeElement).toBe(options.at(0).element)

    // (fail to) move back up again
    await options.at(0).trigger('keydown.up')
    expect(document.activeElement).toBe(options.at(0).element)

    // close the dropdown
    await options.at(0).trigger('keydown.esc')
    expect(dropdown.element).not.toBeVisible()

    expect(document.activeElement).toBe(select.element)
  })
  describe('Select mode', () => {
    test('accepts object options', async () => {
      const wrapper = mount(SaSelect, {
        propsData: {
          options: [
            { label: 'Option 1', value: 1 },
            { label: 'Option 2', value: 2 }
          ]
        }
      })
      await testOptions(wrapper, 0, 1, [2])
    })
    test('accepts array options', async () => {
      const wrapper = mount(SaSelect, {
        propsData: {
          options: ['Option 1', 'Option 2']
        }
      })
      await testOptions(wrapper, 0, 1, ['Option 2'])
    })
    test('changes and clears selection', async () => {
      const wrapper = mount(SaSelect, {
        propsData: {
          options: ['Option 1', 'Option 2']
        }
      })
      const items = wrapper.findAll('.option')
      // select option 2
      await items.at(1).trigger('click')
      expect(wrapper.emitted().change[0]).toStrictEqual([['Option 2']])
      // select option 1 as new selection
      await items.at(0).trigger('click')
      expect(wrapper.emitted().change[1]).toStrictEqual([['Option 1']])
      // clear selection
      await wrapper.find('.clear-select').trigger('click')
      expect(wrapper.find('span.fal.check').exists()).toBe(false)
      expect(wrapper.emitted().change[2]).toStrictEqual([[]])
    })
  })
  describe('Multiselect mode', () => {
    test('changes and clears selection', async () => {
      const wrapper = mount(SaSelect, {
        propsData: {
          options: ['Option 1', 'Option 2'],
          multiselect: true,
          label: 'Some options'
        }
      })
      const items = wrapper.findAll('.option')
      const select = wrapper.find('.select')
      // select option 2
      await items.at(1).trigger('click')
      expect(wrapper.emitted().change[0]).toStrictEqual([['Option 2']])
      // select option 1 and add to selected array
      await items.at(0).trigger('click')
      expect(wrapper.emitted().change[1]).toStrictEqual([
        ['Option 1', 'Option 2']
      ])
      expect(select.text()).toBe('2 Some options')

      // select option 1 again to remove from array
      await items.at(1).trigger('click')
      expect(wrapper.emitted().change[2]).toStrictEqual([['Option 1']])
      expect(select.text()).toBe('Option 1')

      // clear selection
      await wrapper.find('.clear-select').trigger('click')
      expect(select.text()).toBe('Some options')
      expect(wrapper.emitted().change[3]).toStrictEqual([[]])
    })
  })

  test('typeahead enabled', async () => {
    const wrapper = mount(SaSelect, {
      propsData: {
        maxOptions: 8,
        options: Array.from({ length: 100 }).map(
          (item, index) => 'Option ' + index
        ),
        typeahead: true,
        label: 'Some options'
      }
    })
    const typeahead = wrapper.find('.typeahead')

    expect(wrapper.findAll('.option').length).toBe(8)

    await typeahead.find('.input').trigger('click')
    await typeahead.find('input').setValue('33')
    await wait(10)
    const filteredItems = wrapper.findAll('.option')
    expect(filteredItems.length).toBe(1)
    expect(filteredItems.at(0).text()).toBe('Option 33')

    await typeahead.find('.clear').trigger('click')
    await wait(10)
    const items = wrapper.findAll('.option')
    expect(items.length).toBe(8)
    expect(items.at(0).text()).toBe('Option 0')
  })
})

const testOptions = async (
  wrapper,
  unselectedIndex,
  selectedIndex,
  expectedValue
) => {
  const items = wrapper.findAll('.option')
  expect(items.at(0).text()).toBe('Option 1')
  expect(items.at(1).text()).toBe('Option 2')

  const unselected = items.at(unselectedIndex)
  const selected = items.at(selectedIndex)
  await selected.trigger('click')

  expect(unselected.classes()).not.toContain('checked')
  expect(selected.classes()).toContain('checked')
  expect(unselected.find('.icon').exists()).toBe(false)
  expect(selected.find('.icon').exists()).toBe(true)
  expect(wrapper.emitted().change[0]).toStrictEqual([expectedValue])
}

const wait = (ms) => new Promise((resolve) => setTimeout(resolve, ms))
