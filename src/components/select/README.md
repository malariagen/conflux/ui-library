

```vue
<sa-select label="String options" :options="['One', 'Two', 'Three']" />
<sa-select
  label="Object options"
  :options="[
    { value: 'apple', label: 'Apples' },
    { value: 'orange', label: 'Oranges' }
  ]"
/>
```

## Select sizes

```vue
<sa-select size="small" :options="dummyOptions" />
<sa-select :options="dummyOptions" />
<sa-select size="large" :options="dummyOptions" />
<sa-select full-width :options="dummyOptions" />
```

## With Typeahead

```vue
<sa-select :options="dummyOptions" typeahead />
```

## MultiSelect

```vue
<sa-select multiselect :options="dummyOptions" label="Select options" />

<sa-select
  multiselect
  :options="dummyOptions"
  typeahead
  label="Select with typeahead"
/>
```

## Right aligned

```vue
<div class="right-align">
  <sa-select align="right" :options="dummyOptions" />
</div>
```
