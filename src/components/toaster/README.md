Displays "toast" messages that appear over the UI. The component does not deal
with showing and hiding of messages, that needs to be handled elsewhere as shown
in the example below.

The `toasts` prop expects an array of objects, each of which has a `message` and
`type` property where the type is one of `primary` (default), `success` or
`danger`.

```vue
<template>
  <div>
    <sa-toaster :toasts="toasts" @closedtoast="removeToast" />

    <p>Click a button then look up there ↗️</p>
    <sa-button
      size="small"
      text="Show toast"
      @click="addToast('A normal toast', '')"
    />
    <sa-button
      size="small"
      text="Show error toast"
      @click="addToast('An error toast', 'danger')"
    />
    <sa-button
      size="small"
      text="Show success toast"
      @click="addToast('A success toast', 'success')"
    />
  </div>
</template>

<script>
export default {
  data() {
    return {
      toasts: []
    }
  },
  methods: {
    addToast(message, type, expire = 10000) {
      const toast = { message, type }
      this.toasts.push(toast)
      setTimeout(() => this.removeToast(toast), expire)
    },
    removeToast(toast) {
      const index = this.toasts.indexOf(toast)
      if (index > -1) {
        this.toasts.splice(index, 1)
      }
    }
  }
}
</script>
```
