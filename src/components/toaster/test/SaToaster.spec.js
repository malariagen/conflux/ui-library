import { mount } from '@vue/test-utils'
import SaToaster from '@/components/toaster/SaToaster'

describe('SaToaster', () => {
  test('it works', () => {
    const wrapper = mount(SaToaster)
    expect(wrapper).toBeTruthy()
  })
  test('it shows toasts', async () => {
    const wrapper = mount(SaToaster, {
      propsData: {
        toasts: [
          {
            message: 'test message',
            type: 'danger'
          },
          {
            message: 'second message'
          }
        ]
      }
    })
    const captions = wrapper.findAll('.caption')
    expect(captions.length).toBe(2)
    expect(captions.at(0).text()).toBe('test message')
    expect(captions.at(0).classes()).toEqual(['caption', 'danger'])
    expect(captions.at(1).text()).toBe('second message')
    expect(captions.at(1).classes()).toEqual(['caption'])

    await wrapper
      .findAll('.toast')
      .at(1)
      .find('.hide')
      .trigger('click')
    expect(wrapper.emitted('closedtoast')).toEqual([
      [{ message: 'second message' }]
    ])
  })
})
