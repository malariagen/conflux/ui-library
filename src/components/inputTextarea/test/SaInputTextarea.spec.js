import { mount } from '@vue/test-utils'
import SaInputTextarea from '@/components/inputTextarea/SaInputTextarea'

describe('SaInputText', () => {
  test('it works', () => {
    const wrapper = mount(SaInputTextarea)
    expect(wrapper).toBeTruthy()
  })
  test('it renders with default props', () => {
    const wrapper = mount(SaInputTextarea)
    const input = wrapper.find('.textarea')
    expect(input.classes()).toContain('regular')
    expect(input.element.rows).toBe(4)
  })
  test('it renders disabled class', () => {
    const wrapper = mount(SaInputTextarea, {
      propsData: {
        disabled: true
      }
    })
    const input = wrapper.find('.textarea')
    expect(input.classes()).toContain('disabled')
    expect(input.element.disabled).toBe(true)
  })
  test('it renders error class', () => {
    const wrapper = mount(SaInputTextarea, {
      propsData: {
        inError: true
      }
    })
    const input = wrapper.find('.textarea')
    expect(input.classes()).toContain('error')
  })
})
