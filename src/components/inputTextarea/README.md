## Sizes

```vue
<sa-input-textarea size="small" :rows="2" placeholder="Small Text Area" />
<sa-input-textarea placeholder="Regular Text Area" />
<sa-input-textarea size="large" :rows="8" placeholder="Large Text Area" />
```

## Variants

```vue
<sa-input-textarea disabled placeholder="This is disabled" />
<sa-input-textarea in-error placeholder="Error" />
```
