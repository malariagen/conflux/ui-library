import { mount } from '@vue/test-utils'
import SaInputFile from '@/components/inputFile/SaInputFile'

describe('SaInputFile', () => {
  test('renders with default props', () => {
    const wrapper = mount(SaInputFile)
    expect(wrapper).toBeTruthy()
    expect(wrapper.find('.text').element).toContainHTML('Upload a file')
    expect(wrapper.find('[type="text"]').element.value).toEqual('')
  })
  test('it takes the file from the event', () => {
    const emit = jest.fn()
    const wrapper = mount(SaInputFile)
    const file = {
      type: 'image/png',
      size: 1000,
      name: 'image.png'
    }
    const event = {
      target: {
        files: [file]
      }
    }
    wrapper.vm.$emit = emit
    wrapper.vm.onFileChange(event)
    expect(wrapper.vm.$data.filename).toEqual('image.png')
    expect(emit).toHaveBeenCalledWith('input', file)
  })
  test('handles cancel button', () => {
    const emit = jest.fn()
    const wrapper = mount(SaInputFile)
    const event = {
      target: {
        files: []
      }
    }
    wrapper.vm.$emit = emit
    wrapper.vm.onFileChange(event)
    expect(wrapper.vm.$data.filename).toBeNull()
    expect(emit).toHaveBeenCalledWith('input', undefined)
  })
  test('button opens dialog', () => {
    const clickInput = jest.fn()
    const wrapper = mount(SaInputFile)
    wrapper.vm.$refs.input.click = clickInput
    wrapper.find('[role="button"]').trigger('keydown.enter')
    expect(clickInput).toHaveBeenCalled()
  })
})
