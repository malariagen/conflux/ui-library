import { mount } from '@vue/test-utils'
import SaModal from '@/components/modal/SaModal'

describe('SaModal', () => {
  test('it works with minimal props', () => {
    const wrapper = mount(SaModal, {
      propsData: {
        title: 'test'
      }
    })
    expect(wrapper).toBeTruthy()
    const modal = wrapper.find('.modal')
    expect(modal.classes()).toEqual(['modal'])
  })

  test('has right full-width class', () => {
    const wrapper = mount(SaModal, {
      propsData: {
        title: 'test',
        fullWidth: true
      }
    })

    const modal = wrapper.find('.modal')
    expect(modal.classes()).toEqual(['modal', 'full-width'])
  })

  test('adds/removes body class when shown/hidden', async () => {
    const wrapper = mount(SaModal, {
      propsData: {
        title: 'test'
      }
    })
    wrapper.vm.openModal()
    expect(document.body.classList).toContain('noscroll')

    wrapper.vm.closeModal()
    wrapper.trigger('transitionend') // normally this would be triggered by the transition element in the component
    await wrapper.vm.$nextTick()

    expect(document.body.classList).not.toContain('noscroll')
  })

  test('makes no changes to body when no body class', async () => {
    function getBodyClasses() {
      return [...document.body.classList]
    }

    const originalClasses = getBodyClasses()
    const wrapper = mount(SaModal, {
      propsData: {
        title: 'test',
        bodyClass: null
      }
    })
    expect(getBodyClasses()).toEqual(originalClasses)

    wrapper.destroy()
    wrapper.trigger('transitionend') // normally this would be triggered by the transition element in the component
    await wrapper.vm.$nextTick()

    expect(getBodyClasses()).toEqual(originalClasses)
  })

  test('fires close event when background is clicked', () => {
    const wrapper = mount(SaModal, {
      propsData: {
        title: 'test',
        bodyClass: null
      }
    })

    wrapper.vm.openModal()

    const background = wrapper.find('.background-fill')
    background.trigger('click')
    expect(wrapper.emitted().close).toBeTruthy()
  })

  test('fires close event when escape is pressed', () => {
    const wrapper = mount(SaModal, {
      propsData: {
        title: 'test',
        bodyClass: null
      }
    })

    wrapper.vm.openModal()

    const background = wrapper.find('.background-fill')
    background.trigger('keydown.esc')
    expect(wrapper.emitted().close).toBeTruthy()
  })

  test('does not fire close event in strict mode when background is clicked', () => {
    const wrapper = mount(SaModal, {
      propsData: {
        title: 'test',
        bodyClass: null,
        strict: true
      }
    })

    wrapper.vm.openModal()

    const background = wrapper.find('.background-fill')
    background.trigger('click')
    expect(wrapper.emitted().close).toBeFalsy()
  })

  test('does not fire close event in strict mode when escape is pressed', () => {
    const wrapper = mount(SaModal, {
      propsData: {
        title: 'test',
        bodyClass: null,
        strict: true
      }
    })

    wrapper.vm.openModal()

    const background = wrapper.find('.background-fill')
    background.trigger('keydown.esc')
    expect(wrapper.emitted().close).toBeFalsy()
  })

  test('responds to toggle events for the right id', () => {
    const wrapper = mount(SaModal, {
      propsData: {
        id: 'test-modal',
        title: 'test'
      }
    })

    wrapper.vm.$root.$emit('modal-toggle', 'test-modal')
    expect(wrapper.emitted()).toEqual({ open: [[]] })

    // should do nothing
    wrapper.vm.openModal()
    expect(wrapper.emitted()).toEqual({ open: [[]] })

    wrapper.vm.$root.$emit('modal-toggle', 'test-modal')
    expect(wrapper.emitted()).toEqual({ open: [[]], close: [[]] })

    // should do nothing
    wrapper.vm.closeModal()
    expect(wrapper.emitted()).toEqual({ open: [[]], close: [[]] })
  })

  test('does not respond to toggle events for the wrong id', () => {
    const wrapper = mount(SaModal, {
      propsData: {
        id: 'test-modal',
        title: 'test'
      }
    })
    wrapper.vm.$root.$emit('modal-toggle', 'another-modal')
    expect(wrapper.emitted()).toEqual({})
  })
})
