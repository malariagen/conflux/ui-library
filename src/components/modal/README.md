## Simple modal

```vue
<sa-modal id="modal-1" title="Simple modal">
  <template v-slot:body>
    <p>Click outside or press escape key to close</p>
  </template>
</sa-modal>
<sa-button text="Open modal" modal-id="modal-1" />
```

## Full width modal with footer

```vue
<sa-modal id="big-modal" title="Big modal" full-width>
  <template v-slot:body>
    <p>
      Integer et ligula ac elit interdum pharetra. Phasellus interdum at
      erat non lacinia. Nam dolor eros, bibendum sed felis eu, luctus
      accumsan mi. Quisque dignissim, augue a dapibus tincidunt, leo lacus
      euismod sem, et accumsan ligula nisi efficitur augue. Mauris et
      porttitor nibh. Morbi rutrum facilisis erat ac facilisis. Duis id nisi
      turpis. Vivamus rhoncus blandit sem. Donec molestie mauris non justo
      condimentum elementum. Curabitur sodales rhoncus ornare.
    </p>
    <p>
      Pellentesque pretium, neque quis pharetra aliquet, libero leo faucibus
      enim, vulputate ullamcorper turpis ligula ac nunc. In at est non leo
      aliquet sagittis in id magna. Nullam hendrerit ante est, et blandit mi
      ullamcorper id. Nulla nibh leo, ornare vitae sollicitudin non, dapibus
      sed nisi. Duis at augue purus. Donec aliquam tincidunt erat, nec
      suscipit velit aliquet ut. Praesent id erat nec justo lacinia mollis.
      Pellentesque quis mi ut ligula ultrices laoreet. Sed condimentum
      tristique varius. Nam iaculis euismod orci a mattis. Morbi dictum est
      malesuada ipsum aliquam rutrum. Morbi eleifend massa diam, a
      consectetur orci feugiat sed.
    </p>
  </template>
  <template v-slot:footer>
    <div style="display: flex; justify-content: space-between;">
      <sa-button text="Action" />
      <div style="display: flex">
        <sa-button text="Save" modal-id="big-modal" />
        <sa-button
          text="Cancel"
          type="danger"
          style="margin-left: 0.5rem;"
          modal-id="big-modal"
        />
      </div>
    </div>
  </template>
</sa-modal>
<sa-button text="Open modal" modal-id="big-modal" />
```
