import './assets/scss/global.scss'

import * as datePicker from '@duetds/date-picker/dist/loader'
import customParseFormat from 'dayjs/plugin/customParseFormat'
import dayjs from 'dayjs'

import * as components from './components'

export default {
  install(Vue) {
    Vue.mixin({
      methods: {
        isEmpty(someObject) {
          return (
            !someObject ||
            (typeof someObject === 'object' &&
              Object.keys(someObject).length === 0)
          )
        },
        formatNumber(value) {
          if (isNaN(value)) {
            return value
          } else if (
            // if I have more than 3 decimal places, treat me as a string to preserve my precision
            value.toString().split('.').length > 1 &&
            value.toString().split('.')[1].length > 3
          ) {
            return value
          } else {
            // add thousands seperators
            return value.toLocaleString('en')
          }
        },
        makeUniqueId() {
          return `${this.$options.name}-${Math.random()
            .toString(36)
            .slice(2, 6)}`
        },
        pluralize(name, number, suffix = 's') {
          return `${name}${number === 1 ? '' : suffix}`.replace(
            '%',
            this.formatNumber(number)
          )
        }
      }
    })

    Vue.directive('clickAway', {
      // from https://stackoverflow.com/a/42389266/132208
      bind(el, binding, vnode) {
        el.clickOutsideEvent = (event) => {
          if (el !== event.target && !el.contains(event.target)) {
            vnode.context[binding.expression](event)
          }
        }
        document.body.addEventListener('click', el.clickOutsideEvent)
      },
      unbind(el) {
        document.body.removeEventListener('click', el.clickOutsideEvent)
      }
    })

    Object.values(components).forEach((c) => {
      Vue.component(c.name, c)
    })

    Vue.config.ignoredElements = [/duet-\w*/]
    datePicker.defineCustomElements(window)
    dayjs.extend(customParseFormat)
  }
}

export createTableStore from './store/createTableStore'
export createSearchStore from './store/createSearchStore'
export tableStoreMixin from './mixins/tableStore'
export { getColumnValue } from './data/util'
export * from './data/constants'
