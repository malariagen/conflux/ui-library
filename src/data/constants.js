export const COLUMN_GROUPS = {
  Status: 'status',
  Dates: 'dates'
}

export const DATATYPES = {
  ID: 'ID',
  Event: 'Event',
  Date: 'Date',
  Status: 'Status',
  Statuses: 'Statuses',
  Boolean: 'Boolean',
  Number: 'Number',
  Array: 'Array',
  Disabled: 'Disabled'
}

export const FILTER_TYPES = {
  Select: 'Select',
  MultiSelect: 'MultiSelect',
  Select_Typeahead: 'Select_TA',
  MultiSelect_Typeahead: 'MultiSelect_TA',
  Multisearch: 'Multisearch',
  DateRange: 'DateRange'
}
