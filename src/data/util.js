export const getColumnValue = (row, id) => {
  // if field has key, return value
  if (row[id] !== undefined) {
    return row[id]
  }

  // subfields
  const subfields = Object.values(row).filter((field) =>
    field ? field.constructor === Object : undefined
  )
  for (const subfield of subfields) {
    // if subfield has key return value
    if (subfield[id] !== undefined) {
      return subfield[id]
    }
  }

  // ids
  const ids = row.ids ? row.ids : []
  for (const idObj of ids) {
    // if id matches return value
    if (idObj.id === id) {
      return idObj.value
    }
  }

  // events
  const events = row.events ? row.events : []
  for (const event of events) {
    // if event id matches, return event
    if (event.id === id) {
      return event
    }
    // if started or done id matches, return value
    if (event.started && event.started.id === id) {
      return event.started.value
    } else if (event.done && event.done.id === id) {
      return event.done.value
    }
  }

  return null
}
