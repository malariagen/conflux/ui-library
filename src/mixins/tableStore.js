export default {
  props: {
    /** Name of the Vuex store used to store the table model */
    storeName: {
      type: String,
      required: true
    }
  },
  computed: {
    tableStore() {
      return {
        state: this.$store.state[this.storeName],
        commit: (type, payload) =>
          this.$store.commit(`${this.storeName}/${type}`, payload),
        dispatch: (type, payload) =>
          this.$store.dispatch(`${this.storeName}/${type}`, payload)
      }
    }
  }
}
