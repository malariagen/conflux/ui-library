import Vue from 'vue'
import { COLUMN_GROUPS } from '../data/constants'

export default (dataLoader, columnGroup = COLUMN_GROUPS.Status) => {
  return {
    namespaced: true,
    state: () => ({
      model: null,
      rows: null,
      meta: null,
      filters: {},
      requestId: 1,
      requests: [],
      columnGroup
    }),
    mutations: {
      reset(state) {
        state.rows = null
        state.meta = null
        state.requests = []
      },
      addData(state, { offset, data: { rows, meta }, request }) {
        // only accept data if it is expected (reset will clear list of expected requests)
        const idx = state.requests.indexOf(request)
        if (idx === -1) {
          return
        }

        Vue.delete(state.requests, idx)
        if (!state.rows) {
          state.rows = Array.from({ length: meta.total_filtered_results })
        }
        for (let i = rows.length - 1; i >= 0; i--) {
          Vue.set(state.rows, i + offset, rows[i])
        }
        state.meta = { ...state.meta, ...meta }
      },
      clearFilter(state) {
        state.filters = {}
      },
      setFilters(state, filters) {
        state.filters = filters
      },
      setFilter(state, { filter, selected }) {
        // delete first, then conditionally set, to ensure that observers are notified
        Vue.delete(state.filters, filter)
        if (selected) {
          Vue.set(state.filters, filter, selected)
        }
      },
      setColumnGroup(state, value) {
        state.columnGroup = value
      },
      setModel(state, value) {
        state.model = value
      },
      newRequest(state, request) {
        state.requestId++
        state.requests = [...state.requests, request]
      }
    },
    actions: {
      async loadRows({ commit, state }, { offset, limit }) {
        const includeSummary = !state.meta || !state.meta.summary
        const { requestId } = state
        const request = { requestId, offset, limit }
        commit('newRequest', request)
        const data = await dataLoader.fetch(
          state.filters,
          offset || 0,
          limit,
          null,
          includeSummary,
          requestId
        )
        commit('addData', { data, offset, request })
      },
      async exportData({ commit, state }) {
        const { requestId } = state
        commit('newRequest')
        return await dataLoader.fetchCsv(
          state.filters,
          0,
          state.meta.total_filtered_results,
          null,
          requestId
        )
      },
      async loadModel({ commit }) {
        const model = await dataLoader.loadModel(false)
        commit('setModel', model)
        const modelOptions = await dataLoader.loadModel(true)
        commit('setModel', modelOptions)
      },
      updateFilter({ commit, state }, { filter, selected }) {
        // only set the filter when the selection is different (to avoid unnecessary resets)
        const existing = state.filters[filter]
        let changed = false
        if (selected) {
          changed = JSON.stringify(selected) !== JSON.stringify(existing)
        } else {
          changed = !!existing
        }

        if (changed) {
          commit('setFilter', { filter, selected })
          commit('reset')
        }
      },
      setFilters({ commit, state }, filters) {
        const existing = state.filters
        const changed = JSON.stringify(filters) !== JSON.stringify(existing)
        if (changed) {
          commit('setFilters', filters)
          commit('reset')
        }
      },
      setColumnGroup({ commit }, value) {
        commit('setColumnGroup', value)
      },
      setModel({ commit }, value) {
        commit('setModel', value)
      },
      clearFilter({ commit }) {
        commit('clearFilter')
        commit('reset')
      }
    }
  }
}
