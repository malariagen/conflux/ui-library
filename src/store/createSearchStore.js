import { getColumnValue } from '@/data/util'

export default (dataLoader) => {
  return {
    namespaced: true,
    state: () => ({
      suggestions: [],
      suggestionsTotal: null,
      suggestionsLoading: false,
      suggestionsError: false,
      suggestionsRequestId: 0,

      validatedValues: [],
      validatedError: false,
      validatedRequestId: 0
    }),
    mutations: {
      suggestionsSuccess(state, results) {
        state.suggestions = results.rows
        state.suggestionsTotal = results.meta.total_results
        state.suggestionsLoading = false
      },
      suggestionsReset(state) {
        state.suggestions = []
        state.suggestionsTotal = null
        state.suggestionsError = false
      },
      suggestionsStart(state) {
        state.suggestionsLoading = true
        state.suggestionsError = false
        state.suggestionsRequestId += 1
      },
      suggestionsError(state) {
        state.suggestionsLoading = false
        state.suggestionsError = true
      },
      validateStart(state) {
        state.validatedError = false
        state.validatedRequestId += 1
      },
      validateSuccess(state, data) {
        state.validatedValues = data
      },
      validateError(state) {
        state.validatedError = true
      }
    },
    actions: {
      async getSuggestions(
        { commit, state },
        { filterId, filterValue, suggestionLimit }
      ) {
        if (filterValue) {
          commit('suggestionsStart')
          const { suggestionsRequestId } = state
          try {
            const results = await dataLoader.search(
              filterId,
              filterValue,
              suggestionLimit
            )
            if (suggestionsRequestId === state.suggestionsRequestId) {
              commit('suggestionsSuccess', results, suggestionsRequestId)
            }
          } catch (err) {
            if (suggestionsRequestId === state.suggestionsRequestId) {
              commit('suggestionsError', err)
            }
          }
        } else {
          commit('suggestionsReset')
        }
      },

      async validateValues({ commit, state }, { filterId, filterValues }) {
        commit('validateStart')
        const { validatedRequestId } = state
        try {
          const results = await dataLoader.fetch(
            { [filterId]: filterValues },
            0,
            1000
          )
          if (validatedRequestId === state.validatedRequestId) {
            commit(
              'validateSuccess',
              results.rows.map((row) => getColumnValue(row, filterId))
            )
          }
        } catch (err) {
          if (validatedRequestId === state.validatedRequestId) {
            commit('validateError')
          }
        }
      }
    }
  }
}
