import createStore from './store'

export default (previewComponent) => ({
  store: createStore(),
  render(createElement) {
    return createElement(previewComponent)
  }
})
