import React from 'react'
import SectionHeadingRenderer from 'rsg-components-default/SectionHeading/SectionHeadingRenderer'

export default ({ depth, ...props }) => (
  <SectionHeadingRenderer depth={1} {...props} />
)
