// used by Styleguidist to register icons, mixins and global styles

import Vue from 'vue'
import Vuex from 'vuex'
import Plugin from '@/plugin'

Vue.use(Vuex)
Vue.use(Plugin)

// register styleguide components
const components = require.context('./', true, /\.vue$/i)
components.keys().map((key) => {
  Vue.component(
    components(key).default.name ??
      key
        .split('/')
        .pop()
        .split('.')[0],
    components(key).default
  )
})

Vue.mixin({
  data() {
    return {
      dummyOptions: [
        'Option One',
        'Option Two',
        'Option Three',
        'Long option id dolor id ultricies vehicula ut id elit',
        'Long option id dolor id nibh vehicula ut id elit',
        'Long option id dolor id nibh ultricies ut id elit',
        'Long option id dolor id nibh ultricies vehicula id elit',
        'Long option id dolor id nibh ultricies vehicula ut elit',
        'Long option id dolor id nibh ultricies vehicula ut id',
        'Long option id dolor id nibh ultricies vehicula ut id elit',
        'Long option dolor id nibh ultricies vehicula ut id elit',
        'Long id dolor id nibh ultricies vehicula ut id elit',
        'Option id dolor id nibh ultricdsfsdfsdf sd f sdf s df sdf sd fs df sdf sdf s fsd f sdf sdsdfsdfsdfsdfsdfsdfsdfsdfsf s f sdf sdfies vehicula ut id elit',
        'Long option id nibh ultricies vehicula ut id elit',
        'Long option id dolor id vehicula ut id elit',
        'Long option id dolor id nibh ultricies vehicula ut',
        'Long dolor id nibh ultricies vehicula ut id elit',
        'Long option id dolor id nibh ultricies vehicula elit',
        'Long option id dolor id nibh ut id elit',
        'Long option id dolor id nibh ultricies vehicula ut id it',
        'Long option id dolor id nibh ultricies vehicu ut id elit',
        'Long option id dolor id nibh ultricies vicula ut id eit',
        'Long option id dolor id bh ultricies vehicula ut id elit',
        'Long option id lor id nibh ultricies vehicula ut idt',
        'Long option id dolor id nibh ultricies vehicula utt',
        'Long option id dolor id nibh ultricies vehiculait',
        'Long option id dolor id nibh ultricies vehicuit',
        'Long option id dolor id nibh ultricies vehicelit',
        'Long option id dolor id nibh ultricies vehlit'
      ]
    }
  },
  methods: {
    alert(msg) {
      alert(msg)
    }
  }
})
