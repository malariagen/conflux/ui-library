import Rand from 'random-seed'
import { SAMPLE_DATA } from './sampleModel'
import { getColumnValue } from '@/plugin'

function escapeRegExp(string) {
  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&') // $& means the whole matched string
}

export default class FakeDataLoader {
  constructor(totalRows, model, timeout = 2000) {
    const studies = ['ABC-1', 'ABC-2', 'ABC-3', 'ABC-4']

    this.model = model
    this.random = new Rand('faaaaaaaaaaaaaake')
    this.maxTimeout = timeout
    this.loggingEnabled = false
    this.errorProbablility = 0 // increase for simulated network errors

    const rowTemplate = JSON.stringify(SAMPLE_DATA)

    // create {totalRows} items, using SAMPLE_DATA as the basis, and customising a few fields
    this.rows = Array.from({ length: totalRows }).map((ignored, index) => {
      const randomBool = this.random(10) < 6
      const randomNum = this.random.intBetween(0, 50)
      const study = studies[this.random(studies.length)]

      const row = JSON.parse(rowTemplate)

      // ID Field
      row.ids[0].value = study + '.' + (index + 1)

      // Event Field
      let sample = {}
      sample = {
        id: 'event_field',
        status: randomBool ? 'complete' : 'pending',
        started: {
          id: 'event_field_start',
          value: this.makeDateString(randomNum)
        },
        done: {
          id: 'event_field_end',
          value: randomBool
            ? this.makeDateString(this.random.intBetween(randomNum, 50))
            : null
        }
      }
      row.events[0] = sample

      // Date Field
      if (!randomBool) {
        row.date_field = null
      } else {
        row.date_field = this.makeDateString(
          this.random.intBetween(100 * index, 100 * (index + 1))
        )
      }

      // Status Field
      if (randomBool) {
        row.status_field = 'complete'
      } else {
        row.status_field = 'pending'
      }

      // Statuses Field
      row.statuses_field.statuses = [
        {
          status: 'pending',
          count: randomNum
        },
        {
          status: 'complete',
          count: randomNum + index
        }
      ]

      // Boolean Field
      row.boolean_field = randomBool

      // Number Field
      row.number_field = this.random.intBetween(1000, 9999)

      // Array Field
      row.array_field = studies.slice(this.random.intBetween(0, 4), 4)

      // Disabled Field
      // can have any content and will not be visible on frontend

      // Subfield
      if (randomBool) {
        row.subfield.subfield_1 = 'XYZ'
        row.subfield.subfield_2 = 'ABC'
      }

      return row
    })
  }

  // creates a date the given number of days after a fixed date
  makeDateString(daysOffset) {
    const date = new Date(2020, 6, 1, 12, 0, 0, 0)
    date.setDate(date.getDate() + daysOffset)
    return date.toISOString()
  }

  // logs debugging information, as we have no network data to look at.
  log() {
    if (this.loggingEnabled) {
      // eslint-disable-next-line no-console
      console.log(...arguments)
    }
  }

  async simulateNetwork() {
    if (this.maxTimeout) {
      await new Promise((resolve) =>
        setTimeout(resolve, this.random(this.maxTimeout))
      )
    }

    if (Math.random() < this.errorProbablility) {
      this.log('triggering fake error')
      throw new Error('fake error')
    }
  }

  async loadModel() {
    this.log('loading model', this.model)
    await this.simulateNetwork()

    return this.model
  }

  async fetchCsv(filters, offset, limit, orderBy, requestId) {
    await this.simulateNetwork()

    const data = await this.fetch(
      filters,
      offset,
      limit,
      orderBy,
      false,
      requestId
    )

    // quick and dirty. this isn't guaranteed to actually be a valid csv, but will be close
    const csv = []
    const fields = []
    this.model.fields.forEach((field) => {
      if (!field.subfields) {
        fields.push(field)
      } else {
        field.subfields.forEach((subfield) => {
          if (field.title) {
            subfield = {
              ...subfield
            }
            subfield.title = field.title + ': ' + subfield.title
          }
          if (field.name) {
            subfield = {
              ...subfield
            }
            subfield.name = field.name + '.' + subfield.name
          }

          fields.push(subfield)
        })
      }
    })
    csv.push(fields.map((field) => field.title).join(','))
    data.rows.forEach((row) => {
      const line = []
      fields.forEach((field) => {
        const value = getColumnValue(row, field.name)
        if (value) {
          let string = JSON.stringify(value)
          if (string[0] !== '"' && string.includes(',')) {
            string = '"' + string.replace(/"/g, '""') + '"'
          }
          line.push(string)
        } else {
          line.push('')
        }
      })
      csv.push(line.join(','))
    })
    return csv.join('\n')
  }

  async fetch(filters, offset, limit, orderBy, includeSummary, requestId) {
    this.log('loading rows', {
      filters,
      offset,
      limit,
      orderBy,
      includeSummary,
      requestId
    })
    await this.simulateNetwork()

    const filtered = this.rows.filter((row) => {
      for (const key of Object.keys(filters)) {
        if (key === 'q') {
          const id = getColumnValue(row, 'id_field')
          return id.includes(filters[key])
        }
        if (key === 'daterange') {
          const filter = filters.daterange
          const date = getColumnValue(row, filter.field)
          let keep = !!date
          if (filter.from) {
            keep &= date >= filter.from
          }
          if (filter.to) {
            keep &= date <= filter.to
          }
          return keep
        }

        const values = filters[key]
        const value = getColumnValue(row, key)
        if (Array.isArray(value) && values.some((i) => value.includes(i))) {
          return true
        }
        if (!values.includes(value)) {
          return false
        }
      }
      return true
    })

    const result = {
      meta: {
        total_results: this.rows.length,
        total_filtered_results: filtered.length,
        offset,
        limit,
        order_by: orderBy,
        request_id: requestId
      },
      rows: filtered.slice(offset, offset + limit)
    }

    if (includeSummary) {
      result.meta.summary = filtered.reduce(
        (summary, row) => {
          summary.ids = combineIds(summary.ids, row.ids)
          summary.events = combineEvents(summary.events, row.events)
          summary.date_field.total += row.date_field ? 1 : 0
          summary.status_field.statuses[0].count +=
            row.status_field === 'pending' ? 1 : 0
          summary.status_field.statuses[1].count +=
            row.status_field === 'complete' ? 1 : 0
          summary.statuses_field.statuses[0].count +=
            row.statuses_field.statuses[0].count
          summary.statuses_field.statuses[1].count +=
            row.statuses_field.statuses[1].count
          summary.boolean_field.total += row.boolean_field ? 1 : 0
          summary.number_field.total += row.number_field
          summary.array_field.total += row.array_field.length ? 1 : 0
          summary.subfield_1.total += row.subfield.subfield_1 ? 1 : 0
          summary.subfield_2.total += row.subfield.subfield_2 ? 1 : 0
          return summary
        },
        {
          ids: [
            {
              id: 'id_field',
              value: { total: 0 }
            }
          ],
          events: [
            {
              id: 'event_field',
              started: {
                id: 'event_field_start',
                value: { total: 0 }
              },
              done: {
                id: 'event_field_end',
                value: { total: 0 }
              },
              statuses: [
                {
                  status: 'pending',
                  count: 0
                },
                {
                  status: 'complete',
                  count: 0
                }
              ]
            }
          ],
          date_field: { total: 0 },
          status_field: {
            statuses: [
              {
                status: 'pending',
                count: 0
              },
              {
                status: 'complete',
                count: 0
              }
            ]
          },
          statuses_field: {
            statuses: [
              {
                status: 'pending',
                count: 0
              },
              {
                status: 'complete',
                count: 0
              }
            ]
          },
          boolean_field: { total: 0 },
          number_field: { total: 0 },
          array_field: { total: 0 },
          subfield_1: { total: 0 },
          subfield_2: { total: 0 }
        }
      )
    }

    return result
  }

  async search(fieldId, term, limit, requestId) {
    this.log('loading search suggestions', fieldId, term, limit, requestId)
    await this.simulateNetwork()

    const anywhereMatch = new RegExp(escapeRegExp(term), 'i')
    const startMatch = new RegExp(`$${escapeRegExp(term)}`, 'i')
    const matches = this.rows
      .map((row) => getColumnValue(row, fieldId))
      .filter((value) => anywhereMatch.test(value))
      .sort((a, b) => {
        const aStart = startMatch.test(a)
        const bStart = startMatch.test(b)
        if (aStart && !bStart) return -1
        if (bStart) return 1
        return a.length === b.length ? a.localeCompare(b) : a.length - b.length
      })

    return {
      meta: {
        total_results: matches.length,
        limit,
        request_id: requestId
      },
      rows: matches.slice(0, limit)
    }
  }
}

const combineEvents = (summaryEvents = [], rowEvents = []) => {
  const combinedEvents = [...summaryEvents]
  const tempRowEvents = [...rowEvents]
  while (tempRowEvents.length) {
    const rowEvent = tempRowEvents.shift()

    let combinedEvent = combinedEvents.find(
      (event) => event.id === rowEvent.id && !event.matched
    )
    if (!combinedEvent) {
      combinedEvent = {
        id: rowEvent.id,
        statuses: [],
        started: {
          id: rowEvent.started.id,
          value: {
            total: 0
          }
        },
        done: {
          id: rowEvent.done.id,
          value: {
            total: 0
          }
        }
      }
      combinedEvents.push(combinedEvent)
    }
    // Record match to support duplicate events
    combinedEvent.matched = true
    combinedEvent.started.value.total += rowEvent.started.value ? 1 : 0
    combinedEvent.done.value.total += rowEvent.done.value ? 1 : 0

    let statusTotal = combinedEvent.statuses.find(
      (statusTotal) => statusTotal.status === rowEvent.status
    )
    if (!statusTotal) {
      statusTotal = {
        status: rowEvent.status,
        count: 0
      }
      combinedEvent.statuses.push(statusTotal)
    }
    statusTotal.count++
  }
  for (const event of combinedEvents) {
    delete event.matched
  }
  return combinedEvents
}

const combineIds = (summaryIds = [], rowIds = []) => {
  const combinedIds = [...summaryIds]
  const tempRowIds = [...rowIds]
  while (tempRowIds.length) {
    const rowId = tempRowIds.shift()
    let combinedId = combinedIds.find(
      (item) => item.id === rowId.id && !item.matched
    )
    if (!combinedId) {
      combinedId = {
        id: rowId.id,
        value: {
          total: 0
        }
      }
      combinedIds.push(combinedId)
    }
    combinedId.value.total += rowId.value ? 1 : 0
  }
  return combinedIds
}
