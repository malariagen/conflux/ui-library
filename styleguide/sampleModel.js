import { DATATYPES, FILTER_TYPES } from '@/plugin'

export const SAMPLE_MODEL_RESPONSE = {
  filters: [
    {
      fieldIds: ['id_field'],
      type: FILTER_TYPES.Multisearch
    },
    {
      fieldIds: ['date_field', 'event_field_start', 'event_field_end'],
      type: FILTER_TYPES.DateRange
    },
    {
      fieldIds: ['array_field'],
      type: FILTER_TYPES.MultiSelect,
      options: ['ABC-1', 'ABC-2', 'ABC-3', 'ABC-4']
    },
    {
      fieldIds: ['boolean_field'],
      type: FILTER_TYPES.Select,
      options: [
        { value: true, label: 'True' },
        { value: false, label: 'False' }
      ]
    }
  ],
  fields: [
    {
      id: 'id_field',
      title: 'ID Field',
      type: `${DATATYPES.ID}`
    },
    {
      id: 'event_field',
      title: 'Event Field',
      type: DATATYPES.Event,
      started: {
        id: 'event_field_start'
      },
      done: {
        id: 'event_field_end'
      }
    },
    {
      id: 'date_field',
      title: 'Date Field',
      type: DATATYPES.Date
    },
    {
      id: 'status_field',
      title: 'Status Field',
      type: DATATYPES.Status
    },
    {
      id: 'statuses_field',
      title: 'Statuses Field',
      type: DATATYPES.Statuses
    },
    {
      id: 'boolean_field',
      title: 'Boolean Field',
      type: DATATYPES.Boolean
    },
    {
      id: 'number_field',
      title: 'Number Field',
      type: DATATYPES.Number
    },
    {
      id: 'array_field',
      title: 'Array Field',
      type: DATATYPES.Array
    },
    {
      id: 'disabled_field',
      title: 'Disabled Field',
      type: DATATYPES.Disabled
    },
    {
      id: 'subfield',
      title: 'Subfield',
      subfields: [
        {
          id: 'subfield_1',
          title: 'Subfield One'
        },
        {
          id: 'subfield_2',
          title: 'Subfield Two'
        }
      ]
    }
  ]
}

export const SAMPLE_DATA = {
  ids: [
    {
      id: 'id_field',
      value: 'ABC-1234'
    }
  ],
  events: [
    // added by the dataloader
    // {
    //   id: 'event_field',
    //   started: {
    //     id: 'event_field_start',
    //     value: '2000-01-01T00:00:00.000Z'
    //   },
    //   done: {
    //     id: 'event_field_end',
    //     value: '2000-01-01T00:00:00.000Z'
    //   },
    //   status: 'complete'
    // },
  ],
  date_field: '2000-01-01T00:00:00.000Z',
  status_field: 'complete',
  statuses_field: {
    statuses: [
      {
        status: 'pending',
        count: 1234
      },
      {
        status: 'complete',
        count: 1234
      }
    ]
  },
  boolean_field: true,
  number_field: 1234,
  array_field: ['One', 'Two', 'Three', 'Four'],
  disabled_field: 'ABCXYZ',
  subfield: {
    subfield_1: 'ABC',
    subfield_2: 'XYZ'
  }
}
