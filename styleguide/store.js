import Vuex from 'vuex'
import FakeDataLoader from './fakeDataLoader'
import { SAMPLE_MODEL_RESPONSE } from './sampleModel'
import { createTableStore, createSearchStore } from '@/plugin'

const dataLoader = new FakeDataLoader(100000, SAMPLE_MODEL_RESPONSE)

export default () =>
  new Vuex.Store({
    modules: {
      samples: createTableStore(dataLoader),
      searchFields: createSearchStore(dataLoader)
    }
  })
