> ℹ️ **NOTE: This repository has moved to: https://gitlab.internal.sanger.ac.uk/genomic-surveillance/tools/ui-library**

## Install

Install the component library as a dependency using `npm`.

```shell
npm install @sanger-surveillance-operations/ui-library
```

## Register

Register the components with Vue and import styles to make them globally
available in your app.

```js static
import Vue from 'vue'
import SangerComponents from '@sanger-surveillance-operations/ui-library'
import '@sanger-surveillance-operations/ui-library/dist/ui-library.css'

Vue.use(SangerComponents)
```

## Use

Now you can use them in your own Vue components.

```js
<template>
  <div>
    <sa-button text="A button" />
  </div>
</template>
```

## Contribute

### Project setup

```shell
npm install
```

### Adding a component

1. Create a new component folder by copying the `_component-template` folder
2. Rename the component, test files and their contents by replacing `SaComponent` with `Sa[YourComponentName]` following the structure of the other components.
3. Add the component to `src/components/index.js`

### Start the development server

```shell
npm start
```

### Build for production

```shell
npm run build
```

### Lint and fix files

```shell
npm run lint
```

### Publish a new version

```shell
npm login # if not already logged in
npm version <new version>
git push
npm publish
```

A production build is automatically created before publishing so there's no need
to explicitly run `npm run build`.
