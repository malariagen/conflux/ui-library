module.exports = {
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1',
    '^~/(.*)$': '<rootDir>/src/$1',
    '^vue$': 'vue/dist/vue.common.js',
    '\\.(css|scss)$': 'identity-obj-proxy'
  },
  moduleFileExtensions: ['js', 'vue', 'json'],
  transform: {
    '\\.js$': 'babel-jest',
    '\\.vue$': 'vue-jest',
    '\\.svg$': '<rootDir>/test/svgTransform.js'
  },
  collectCoverage: true,
  collectCoverageFrom: ['<rootDir>/src/**/*.{js,vue}'],
  coveragePathIgnorePatterns: ['_component-template'],
  coverageDirectory: 'test/coverage',
  reporters: ['default'],
  setupFilesAfterEnv: ['./test/jest.setup.js']
}
