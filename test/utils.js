export const removeLineBreaks = (text) => text.replace(/\s+/g, ' ')
