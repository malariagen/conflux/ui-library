module.exports = {
  presets: ['@babel/preset-env'],
  plugins: [
    '@babel/plugin-transform-runtime',
    '@babel/plugin-proposal-export-default-from'
  ],
  overrides: [
    {
      include: '**/*.jsx',
      plugins: ['@babel/plugin-transform-react-jsx']
    }
  ]
}
